/* global _, Ajax, DATA */

var Pane = (function (window, document) {
  var selectedItem = -1;
  var batch = 0;
  /* Initialize screen */
  _('btn-preview-item').addEventListener('click', doPreview, false);
  _('btn-upload').addEventListener('click', uploadRecipients, false);
  _('btn-send').addEventListener('click', sendBatch, false);
  _('recipient-list').addEventListener('click', deleteRecipient, false);
  _('item-list').addEventListener('mouseup', selectItem, false);
  _('message-checkbox').addEventListener('change', toggleMessageBox, false);
  buildItemList();
  buildRecipientList();
  // Select last newsletter
  openItem(-1);

  function buildItemList() {
    var output = [], itemText;
    for (var i = 0; i < DATA.newsletters.length; i++) {
      output.push('<li id="item-' + i + '"');
      if (i === selectedItem) {
        output.push(' class="hd-selected"');
      }
      output.push('>');
      itemText = DATA.newsletters[i].subject;
      if (itemText === '') {
        output.push('&nbsp;');
      } else {
        itemText = itemText.replace('%y', DATA.newsletters[i].year)
                .replace('%e', DATA.newsletters[i].edition);
        output.push(_.encodeForHtml(itemText));
      }

      output.push('</li>');
    }

    _('item-list').innerHTML = output.join('');
  }

  function selectItem(event) {
    var targetItem = parseInt(event.target.id.substr(5), 10);
    openItem(targetItem);
  }

  function openItem(item) {
    if (selectedItem !== -1) {
      _('item-' + selectedItem).className = '';
    }

    if (item === -1) {
      if (DATA.newsletters.length > 0) {
        item = DATA.newsletters.length - 1;
      }
    }

    if (item !== -1) {
      _('item-' + item).className = 'hd-selected';
      _('nws-form').style.display = 'block';
    } else {
      _('nws-form').style.display = 'none';
    }

    selectedItem = item;
  }

  function buildRecipientList() {
    var html = [];
    var i;

    for (i = 0; i < DATA.recipients.length; i++) {
      html.push('<li>');
      html.push(_.encodeForHtml(DATA.recipients[i].address));
      html.push('<span id="delete-' + parseInt(DATA.recipients[i].id) + '">X</span>');
      html.push('</li> ');
    }

    _('no-recipients').innerHTML = DATA.recipients.length;
    _('recipient-list').innerHTML = html.join('');
  }

  function doPreview() {
    if (selectedItem !== -1) {
      window.open('view/' + DATA.newsletters[selectedItem].year +
              '/' + DATA.newsletters[selectedItem].edition);
    }
  }

  function uploadRecipients() {
    var params = {};
    params.action = 'add';
    params.addresses = _('recipients').value;

    Ajax.postJSON('sendmail-rpc',
            params,
            function (obj) {
              if (obj.ok === 1) {
                _('recipients').value = '';
                DATA.recipients = obj.recipients;
                buildRecipientList();
              }
            },
            'Uploading…');
  }

  function deleteRecipient(event) {
    if (event.target.id.substr(0, 6) === 'delete') {
      var params = {};
      params.action = 'delete';
      var id = event.target.id;
      params.recipientId = id.substr(id.indexOf('-') + 1);

      Ajax.postJSON('sendmail-rpc',
              params,
              function (obj) {
                if (obj.ok === 1) {
                  DATA.recipients = obj.recipients;
                  buildRecipientList();
                }
              },
              'Deleting…');
    }
  }

  function toggleMessageBox() {
    var messageBox = _('message');

    if (_('message-checkbox').checked) {
      messageBox.disabled = false;
      if (messageBox.value === '') {
        messageBox.value = DATA.welcomeText;
      }
    } else {
      messageBox.disabled = true;
    }
  }

  function sendBatch() {
    var message = _('message').value;
    var params = {};
    params.action = 'send';
    params.newsletterId = DATA.newsletters[selectedItem].id;

    if (_('message-checkbox').checked && message !== '') {
      params.welcomeText = message;
    }

    Ajax.postJSON('sendmail-rpc',
            params,
            function (obj) {
              if (obj.ok === 1) {
                DATA.recipients = obj.recipients;
                buildRecipientList();
                alert('Sent batch #' + ++batch);
              }
            },
            'Sending…');
  }

  // Exposed functions
  return {
    destroyPane: function (callback) {
      callback();
    }
  };
})(this, this.document);
