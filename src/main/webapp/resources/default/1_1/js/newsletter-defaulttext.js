/* global Ajax, _, CodeMirror, defaultCodeMirrorStyle */

var Pane = (function () {
  /* Initialize screen */
  _('btn-save').addEventListener('click', saveTexts, false);
  var cmHtmlOnline = CodeMirror.fromTextArea(_('html-online'), defaultCodeMirrorStyle);
  var cmHeaderText = CodeMirror.fromTextArea(_('header-text'), defaultCodeMirrorStyle);
  var cmColophonText = CodeMirror.fromTextArea(_('colophon-text'), defaultCodeMirrorStyle);
  var cmFooterText = CodeMirror.fromTextArea(_('footer-text'), defaultCodeMirrorStyle);
  var cmWelcomeText = CodeMirror.fromTextArea(_('welcome-text'), defaultCodeMirrorStyle);
  setTimeout(function () {
    cmHtmlOnline.refresh();
    cmHeaderText.refresh();
    cmColophonText.refresh();
    cmFooterText.refresh();
    cmWelcomeText.refresh();
  }, 0);

  _('nws-form').style.display = 'block';

  function saveTexts() {
    var params = {};
    params.fromName = _('from-name').value;
    params.replyToAddress = _('reply-to').value;
    params.subject = _('subject').value;
    params.plainTextOnline = _('plain-text-online').value;
    params.htmlOnline = cmHtmlOnline.getValue();
    params.headerText = cmHeaderText.getValue();
    params.colophonHead = _('colophon-head').value;
    params.colophonText = cmColophonText.getValue();
    params.footerText = cmFooterText.getValue();
    params.welcomeText = cmWelcomeText.getValue();

    Ajax.postJSON('defaulttexts-rpc',
            params,
            function () {
              /* nothing */
            },
            'Saving…');
  }

  // Exposed functions
  return {
    destroyPane: function (callback) {
      callback();
    }
  };
})();
