/* global DATA, _, Ajax */

var Pane = (function () {
  var selectedItem = -1;
  var changesMade = false;
  /* Initialize screen */
  buildItemList();
  _('btn-save').addEventListener('click', doSave, false);
  _('btn-new-user').addEventListener('click', newUser, false);
  _('btn-del-user').addEventListener('click', deleteUser, false);
  _('username').addEventListener('change', markChanged, false);
  _('full-name').addEventListener('change', markChanged, false);
  _('email').addEventListener('change', markChanged, false);
  _('item-list').addEventListener('mouseup', selectItem, false);

  function buildItemList() {
    var output = [], itemText;
    for (var i = 0; i < DATA.length; i++) {
      output.push('<li id="item-' + i + '"');
      output.push(' class="');
      if (DATA[i].username === undefined) {
        itemText = DATA[i].email;
        output.push('email');
      } else {
        itemText = DATA[i].username;
      }

      if (!DATA[i].active) {
        output.push(' inactive');
      }

      if (i === selectedItem) {
        output.push(' hd-selected');
      }
      output.push('">');
      output.push(_.encodeForHtml(itemText));
      output.push('</li>');
    }

    _('item-list').innerHTML = output.join('');
  }

  function selectItem(event) {
    saveUser(function () {
      var targetItem = parseInt(event.target.id.substr(5), 10);
      openItem(targetItem);
    });
  }

  function openItem(item) {
    if (selectedItem !== -1) {
      _.removeClass(_('item-' + selectedItem), 'hd-selected');
    }

    if (item !== -1) {
      _('item-' + item).className = _('item-' + item).className + ' hd-selected';
      _('username').value = DATA[item].username !== undefined ? DATA[item].username : '';
      _('full-name').value = DATA[item].fullName !== undefined ? DATA[item].fullName : '';
      _('email').value = DATA[item].email;
      _('last-update').innerHTML = new Date(DATA[item].updateTS * 1000).toString();
      _('btn-del-user').disabled = false;
      retrieveSessions(DATA[item].id);
      _('nws-form').style.display = 'block';
    } else {
      _('btn-del-user').disabled = true;
      _('nws-form').style.display = 'none';
    }

    selectedItem = item;
  }

  function markChanged() {
    changesMade = true;
  }

  function doSave() {
    saveUser(function () {
      /* nothing */
    });
  }

  function newUser() {
    saveUser(function () {
      var email = prompt('Enter email address of new user:');

      if (email) {
        var params = {};
        params.action = 'new';
        params.user = {};
        params.user.email = email;
        Ajax.postJSON('accounts-rpc',
                params,
                function (obj) {
                  if (obj.error !== undefined) {
                    alert(obj.error);
                  } else if (obj.ok === 1) {
                    // Add retrieved item to list
                    DATA.push(obj.user);
                    // First rebuild the item list, then open the new item
                    buildItemList();
                    openItem(DATA.length - 1);
                  }
                },
                'Processing…');
      }
    });
  }

  function saveUser(callback) {
    if (changesMade) {
      var params = {};
      params.action = 'save';
      params.user = {};
      params.user.id = DATA[selectedItem].id;
      params.user.username = DATA[selectedItem].username = _('username').value;
      if (params.user.username === '') {  // Don't send empty string
        delete params.user.username;      // instead: NULLify
      }
      params.user.fullName = DATA[selectedItem].fullName = _('full-name').value;
      if (params.user.fullName === '') {
        delete params.user.fullName;
      }
      params.user.email = DATA[selectedItem].email = _('email').value;
      Ajax.postJSON('accounts-rpc',
              params,
              function (obj) {
                if (obj.error !== undefined) {
                  alert(obj.error);
                } else if (obj.ok === 1) {
                  changesMade = false;
                  DATA[selectedItem].username = obj.user.username;
                  DATA[selectedItem].fullName = obj.user.fullName;
                  DATA[selectedItem].email = obj.user.email;
                  DATA[selectedItem].updateTS = obj.user.updateTS;
                  _('last-update').innerHTML = new Date(DATA[selectedItem].updateTS * 1000).toString();
                  buildItemList();
                  callback();
                }
              },
              'Saving…');
    } else {
      callback();
    }
  }

  function deleteUser() {
    var params = {};
    params.action = 'delete';
    params.user = {};
    params.user.id = DATA[selectedItem].id;
    Ajax.postJSON('accounts-rpc',
            params,
            function (obj) {
              if (obj.ok === 1) {
                DATA.splice(selectedItem, 1);
                // First close the right pane, then rebuild the item list
                openItem(-1);
                buildItemList();
              }
            },
            'Processing…');
  }

  function retrieveSessions(userId) {
    _('sessions').innerHTML = '';
    var params = {};
    params.action = 'retrieve-sessions';
    params.userId = userId;
    Ajax.getJSON('accounts-rpc',
            params,
            function (obj) {
              var html = ['Last 10 sessions: '
                        + '<table>'
                        + '<tr>'
                        + '<th>Location (host name)</th>'
                        + '<th>Date/time</th>'
                        + '<th>Active</th>'
                        + '</tr>'];
              var upd;
              var sessions = obj.sessions;

              if (sessions.length !== 0) {
                for (var i = 0; i < sessions.length; i++) {
                  html.push('<tr>');
                  html.push('<td>' + _.encodeForHtml(sessions[i].remoteHost) + '</td>');
                  upd = new Date(sessions[i].updateTS * 1000).toString();
                  html.push('<td>' + upd + '</td>');
                  html.push('<td>' + (sessions[i].active ? '*' : '') + '</td>');
                  html.push('</tr>');
                }

                html.push('</table>');

                _('sessions').innerHTML = html.join('');
              } else {
                _('sessions').innerHTML = '';
              }
            },
            'Retrieving…');
  }

  // Exposed functions
  return {
    destroyPane: function (callback) {
      callback();
    }
  };
})();
