/* global CodeMirror */

function _(id) {
  if (id) {
    return document.getElementById(id);
  }
}

/* remove class using classList if that exists */
// polyfill: https://code.google.com/p/oz-js/source/browse/oz.js#152
_.removeClass = (document.documentElement.classList) ? function (node, className) {
  node.classList.remove(className);
} : function (node, className) {
  var cn = node.className,
          arr = (cn ? cn.split(' ') : []);

  arr = arr.filter(function (c) {
    return c !== className;
  });
  node.className = arr.join(' ');
};

_.clone = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

_.encodeForHtml = function (text) {
  return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
};

Array.prototype.move = function (from, to) {
  this.splice(to, 0, this.splice(from, 1)[0]);
};

var Ajax = (function (window, document, undefined) {
  var messageBox = _('ajax-message');
  // Constants
  var DONE = 4;
  var OK = 200;

  // Private functions
  function serializeParams(params) {
    var paramsArray = [];

    for (var prop in params) {
      paramsArray.push(encodeURIComponent(prop) +
              '=' +
              encodeURIComponent(params[prop]));
    }

    return paramsArray.join('&');
  }

  function setMessage(message) {
    messageBox.firstChild.innerHTML = message;
    messageBox.style.display = 'block';
  }

  function clearMessage() {
    messageBox.style.display = 'none';
  }

  // Exposed functions
  return {
    getJSON: function (url, params, callback, message) {
      var xhr;
      params = serializeParams(params);
      doXhr();

      function doXhr() {
        setMessage(message);
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = readyStateEvent;
        xhr.open('GET', url + '?' + params, true);
        xhr.send();
      }

      function readyStateEvent() {
        if (xhr.readyState === DONE) {
          clearMessage();
          if (xhr.status === OK) {
            var obj = JSON.parse(xhr.responseText);
            if (obj.exception !== undefined) {
              alert(obj.exception);
            } else if (obj.isLoggedOut !== undefined) {
              alert('You have been (remotely) logged out…');
              location.replace(location.href);
            } else {
              callback(obj);
            }
          } else {
            var again = confirm('Status was ' + xhr.status + '.\n' +
                    'Try again?');
            if (again) {
              doXhr();
            }
          }
        }
      }
    },
    postJSON: function (url, params, callback, message) {
      var xhr;
      params = JSON.stringify(params);
      params = encodeURIComponent(params);
      params = 'data=' + params.replace('%20', '+');
      doXhr();

      function doXhr() {
        setMessage(message);
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = readyStateEvent;
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
      }

      function readyStateEvent() {
        if (xhr.readyState === DONE) {
          clearMessage();
          if (xhr.status === OK) {
            var obj = JSON.parse(xhr.responseText);
            if (obj.exception !== undefined) {
              alert(obj.exception);
            } else if (obj.isLoggedOut !== undefined) {
              alert('You have been (remotely) logged out…');
              location.replace(location.href);
            } else {
              callback(obj);
            }
          } else {
            var again = confirm('Status was ' + xhr.status + '.\n' +
                    'Try again?');
            if (again) {
              doXhr();
            }
          }
        }
      }
    },
    setMessage: function (message) {
      setMessage(message);
    },
    clearMessage: function () {
      clearMessage();
    }
  };
})(this, this.document);

function transformTeXToMarkdown(t) {
  return t.replace(/%.*\n/g, '')
          .replace(/\\&/g, '&')
          .replace(/\\ai\//g, 'aï')
          .replace(/\\ee\//g, 'eë')
          .replace(/\\ei\//g, 'eï')
          .replace(/\\ie\//g, 'ië')
          .replace(/\\oe\//g, 'oë')
          .replace(/\\oi\//g, 'oï')
          .replace(/\\oo\//g, 'oö')
          .replace(/\\ui\//g, 'uï')
          .replace(/ \\ /g, ' ␣')
          .replace(/\\(\\| |par\s*)/gm, ' ')
          .replace(/~/g, ' ')
          .replace(/(``|'')/g, '"')
          .replace(/`/g, "'")
          .replace(/\\firapeelpcklk/g, 'Postbus 403, 3500 AK ␣Utrecht')
          // for now, don't use <sup> elements, it breaks the current layout
          //.replace(/{\\sup\s*([^}]*)}/gm, '<sup>$1</sup>')
          .replace(/{\\sup\s*([^}]*)}/gm, '$1')
          // we do not support small capitals in newsletters, yet
          //.replace(/{\\sci?\s*([^}]*)}/gm, '<span class="smcp">$1</span>')
          .replace(/{\\sci?\s*([^}]*)}/gm, function (match, p1) {
            return p1.toUpperCase();
          })
          .replace(/{\\it\s*([^}]*)}/gm, '*$1*')
          .replace(/{\\bfi\s*([^}]*)}/gm, '***$1***')
          .replace(/{\\bf\s*([^}]*)}/gm, '**$1**')
          // eat all other TeX commands and curly braces
          .replace(/(\\[A-Za-z]*\s*|{|})/g, '');
}

CodeMirror.defineMode('markdownCustomHighlight', function (config, parserConfig) {
  var specialCharsOverlay = {
    token: function (stream) {
      if (stream.match('␣')) {
        return 'hard-space';
      }
      if (stream.match('↵')) {
        return 'hard-return';
      }
      while (stream.next() != null && !stream.match('␣', false) &&
              !stream.match('↵', false)) {
        /* nothing */
      }
      return null;
    }
  };
  return CodeMirror.overlayMode(
          CodeMirror.getMode(config, parserConfig.backdrop || 'markdown'),
          specialCharsOverlay);
});

var defaultCodeMirrorStyle = {
  mode: 'markdownCustomHighlight',
  lineWrapping: true,
  extraKeys: {
    Home: 'goLineLeft',
    End: 'goLineRight',
    'Ctrl-Space': function (cm) {
      cm.replaceSelection('␣');
    },
    'Ctrl-Enter': function (cm) {
      cm.replaceSelection('↵');
    }
  }
};
