/* global Ajax, _ */

var Pane = (function () {
  /* Initialize screen */
  _('btn-save').addEventListener('click', saveAppDefaults, false);
  _('nws-form').style.display = 'block';

  function saveAppDefaults() {
    var params = {};
    params.applicationName = _('application-name').value;

    Ajax.postJSON('application-rpc',
            params,
            function () {
              /* nothing */
            },
            'Saving…');
  }

  // Exposed functions
  return {
    destroyPane: function (callback) {
      callback();
    }
  };
})();
