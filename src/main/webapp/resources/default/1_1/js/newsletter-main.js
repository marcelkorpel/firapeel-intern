/* global NWSL_DATA, Ajax, _ */

var Pane = (function () {
  var selectedItem = -1;
  var changesMade = false;
  /* Initialize screen */
  buildItemList();
  _('btn-save').addEventListener('click', doSave, false);
  _('btn-edit-item').addEventListener('click', doEdit, false);
  _('btn-preview-item').addEventListener('click', doPreview, false);
  _('btn-new-item').addEventListener('click', newItem, false);
//  _('btn-del-item').addEventListener('click', deleteItem, false);
  _('year').addEventListener('change', markChanged, false);
  _('edition').addEventListener('change', markChanged, false);
  _('subject').addEventListener('change', markChanged, false);
  _('item-list').addEventListener('mouseup', selectItem, false);
  if (NWSL_DATA.length > 0) {
    openItem(0);
  }

  function buildItemList() {
    var output = [], itemText;
    for (var i = 0; i < NWSL_DATA.length; i++) {
      output.push('<li id="item-' + i + '"');
      if (i === selectedItem) {
        output.push(' class="hd-selected"');
      }
      output.push('>');
      itemText = NWSL_DATA[i].subject;
      if (itemText === '') {
        output.push('&nbsp;');
      } else {
        itemText = itemText.replace('%y', NWSL_DATA[i].year)
                .replace('%e', NWSL_DATA[i].edition);
        output.push(_.encodeForHtml(itemText));
      }

      output.push('</li>');
    }

    _('item-list').innerHTML = output.join('');
  }

  function selectItem(event) {
    saveItem(function () {
      var targetItem = parseInt(event.target.id.substr(5), 10);
      openItem(targetItem);
    });
  }

  function doSave() {
    saveItem(function () {
      /* nothing */
    });
  }

  function doEdit() {
    if (selectedItem !== -1) {
      location.href = 'newsletter/edit/' + NWSL_DATA[selectedItem].id;
    }
  }

  function doPreview() {
    if (selectedItem !== -1) {
      window.open('newsletter/view/' + NWSL_DATA[selectedItem].year +
              '/' + NWSL_DATA[selectedItem].edition);
    }
  }

  function newItem() {
    saveItem(function () {
      // Retrieve an empty item from the server
      var params = {};
      params.action = 'new';

      Ajax.postJSON('newsletter-rpc',
              params,
              function (obj) {
                if (obj.ok === 1) {
                  // Add retrieved item to list
                  NWSL_DATA.push(obj.newsletter);
                  // First, rebuild the item list, then open the new item
                  buildItemList();
                  openItem(NWSL_DATA.length - 1);
                }
              },
              'Loading…');
    });
  }

  function markChanged() {
    changesMade = true;
  }

  function saveItem(callback) {
    if (changesMade) {
      var params = {};
      params.action = 'save';
      params.newsletter = {};
      params.newsletter.id = NWSL_DATA[selectedItem].id;
      params.newsletter.year = NWSL_DATA[selectedItem].year = _('year').value;
      params.newsletter.edition = NWSL_DATA[selectedItem].edition = _('edition').value;
      params.newsletter.subject = NWSL_DATA[selectedItem].subject = _('subject').value;

      Ajax.postJSON('newsletter-rpc',
              params,
              function () {
                changesMade = false;
                buildItemList();
                callback();
              },
              'Saving…');
    } else {
      callback();
    }
  }

  function openItem(item) {
    if (selectedItem !== -1) {
      _('item-' + selectedItem).className = '';
    }

    if (item === -1) {
      if (NWSL_DATA.length > 0) {
        item = 0;
      }
    }

    if (item !== -1) {
      _('item-' + item).className = 'hd-selected';
      _('year').value = NWSL_DATA[item].year;
      _('edition').value = NWSL_DATA[item].edition;
      _('subject').value = NWSL_DATA[item].subject;
      _('nws-form').style.display = 'block';
    } else {
      _('nws-form').style.display = 'none';
    }

    selectedItem = item;
  }

  /* DON'T, really DON'T delete newsletters yet!!!
   function deleteItem() {
   NWSL_DATA.item.splice(selectedItem, 1);
   // first, open the item above the deleted one, then rebuild the item list
   openItem(selectedItem - 1);
   buildItemList();
   }
   */

  // Exposed functions
  return {
    destroyPane: function (callback) {
      saveItem(callback);
    }
  };
})();
