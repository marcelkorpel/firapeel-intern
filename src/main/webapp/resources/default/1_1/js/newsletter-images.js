/* global DATA, _, Ajax, transformTeXToMarkdown, defaultCodeMirrorStyle */

var Pane = (function (window, document, undefined) {
  var emptyItem = {
    id: 0,
    // `name` is deliberately undefined
    localFilename: '',
    deployedFilename: '',
    width: 0,
    height: 0,
    altText: '',
    titleText: '',
    description: ''
  };
  var selectedItem = -1;
  var changesMade = false;
  /* Initialize screen */
  buildItemList();
  _('btn-save').addEventListener('click', saveButton, false);
  _('btn-new-item').addEventListener('click', newItem, false);
//  _('btn-del-item').addEventListener('click', deleteItem, false);
  _('item-list').addEventListener('mouseup', selectItem, false);
  _('name').addEventListener('change', markChanged, false);
  _('alt-text').addEventListener('change', markChanged, false);
  _('title-text').addEventListener('change', markChanged, false);
  _('btn-tex-markdown').addEventListener('click', texToMarkdown, false);
  _('image-file-input').addEventListener('change', uploadImage, false);
  _('image-iframe').addEventListener('load', processImage, false);
  var cmDescription = CodeMirror(_('description-cell'), defaultCodeMirrorStyle);

  function buildItemList() {
    var output = [], itemText;
    for (var i = 0; i < DATA.length; i++) {
      itemText = DATA[i].name;
      output.push('<li id="item-' + i + '" class="');
      if (itemText === undefined) {
        output.push('unnamed ');
      }
      if (i === selectedItem) {
        output.push('hd-selected');
      }
      output.push('">');
      if (itemText === undefined) {
        output.push('(None)');
      } else {
        output.push(_.encodeForHtml(itemText));
      }

      output.push('</li>');
    }

    _('item-list').innerHTML = output.join('');
  }

  function markChanged() {
    changesMade = true;
  }

  function newItem() {
    saveItem(function () {
      // Insert a copy of an empty item at the end of the list
      DATA.push(_.clone(emptyItem));
      // First, rebuild the item list, then open the new item
      buildItemList();
      openItem(DATA.length - 1);
    });
  }

  function selectItem(event) {
    saveItem(function () {
      var targetItem = parseInt(event.target.id.substr(5), 10);
      openItem(targetItem);
    });
  }

  function saveButton() {
    saveItem(function () {
      /* nothing */
    });
  }

  function texToMarkdown() {
    cmDescription.setValue(DATA[selectedItem].description =
            transformTeXToMarkdown(cmDescription.getValue()
                    .replace(/{\\rm\s*([^}]*)}/gm, '{\\it $1}')
                    ));
  }

  function saveItem(callback) {
    if (changesMade) {
      var params = {};
      params.id = DATA[selectedItem].id;
      params.name = DATA[selectedItem].name = _('name').value;
      params.altText = DATA[selectedItem].altText = _('alt-text').value;
      params.titleText = DATA[selectedItem].titleText = _('title-text').value;
      params.description = DATA[selectedItem].description = cmDescription.getValue();
      Ajax.postJSON('images-rpc',
              params,
              function () {
                changesMade = false;
                buildItemList();
                callback();
              },
              'Saving…');
    } else {
      callback();
    }
  }

  function openItem(item) {
    if (selectedItem !== -1) {
      _.removeClass(_('item-' + selectedItem), 'hd-selected');
    }

    if (item === -1) {
      if (DATA.length > 0) {
        item = 0;
      }
    }

    if (item !== -1) {
      _('item-' + item).className += ' hd-selected';
      _('id').value = (DATA[item].id === 0 ? '(none yet)' : DATA[item].id);
      _('name').value = (DATA[item].name === undefined ? '' : DATA[item].name);
      _('alt-text').value = (DATA[item].altText === undefined ? '' : DATA[item].altText);
      _('title-text').value = (DATA[item].titleText === undefined ? '' : DATA[item].titleText);
      previewImage(item);
      _('nws-form').style.display = 'block';
      // Always issue CodeMirror manipulation functions after other DOM changes
      cmDescription.off('change', markChanged);
      cmDescription.setValue(DATA[item].description === undefined ? '' : DATA[item].description);
      cmDescription.on('change', markChanged);
    } else {
      _('nws-form').style.display = 'none';
    }

    selectedItem = item;
  }

  function uploadImage() {
    Ajax.setMessage('Uploading…');
    _('image-id').value = DATA[selectedItem].id;
    _('image-upload-form').submit();
  }

  function processImage() {
    var iframe = _('image-iframe').contentWindow;
    var json = iframe.document.getElementsByTagName('body')[0].innerHTML;
    if (json !== '') {
      var obj = JSON.parse(json);
      console.dir(obj);
      Ajax.clearMessage();
      if (obj.exception !== undefined) {
        alert(obj.exception);
      } else if (obj.isLoggedOut !== undefined) {
        alert('You have been (remotely) logged out…');
        location.replace(location.href);
      } else {
        if (obj.ok === 1) {
          DATA[selectedItem].localFilename = obj.image.localFilename;
          DATA[selectedItem].width = parseInt(obj.image.width, 10);
          DATA[selectedItem].height = parseInt(obj.image.height, 10);

          if (_('name').value === '') {
            _('name').value = obj.image.name;
            markChanged();
          }

          _('image-file-input').value = null;
          _('id').value = DATA[selectedItem].id = obj.image.id;
          previewImage(selectedItem);
          buildItemList();
        }
      }
    }
  }

  function previewImage(item) {
    var html = '';

    if (DATA[item].localFilename) {
      html = 'Size: ' + DATA[item].width + ' x '
              + DATA[item].height + '<br />';
      html += '<img src="../../static/uploads/newsletter/image/'
              + _.encodeForHtml(DATA[item].localFilename) + '" />';
    }

    _('img-preview').innerHTML = html;
  }

  /*
   function deleteItem() {
   DATA.splice(selectedItem, 1);
   // first, open the item above the deleted one, then rebuild the item list
   openItem(selectedItem - 1);
   buildItemList();
   }
   */

  // Exposed functions
  return {
    destroyPane: function (callback) {
      saveItem(callback);
    }
  };
})(this, this.document);
