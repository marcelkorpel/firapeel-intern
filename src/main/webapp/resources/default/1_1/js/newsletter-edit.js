/* global NWSL_DATA, _, Ajax, transformTeXToMarkdown, defaultCodeMirrorStyle */

var Pane = (function (window, document) {
  var emptyItem = {
    headLevel: 2,
    head: '',
    subhead: '',
    bodyText: '',
    sidebarText: '',
    metadataText: ''
  };

  var selectedItem = -1;

  /* Initialize screen */
  buildItemList();
  _('nwsl-save').addEventListener('click', saveItems, false);
  _('btn-preview-item').addEventListener('click', doPreview, false);
  _('btn-new-item').addEventListener('click', newItem, false);
  _('btn-del-item').addEventListener('click', deleteItem, false);
  _('btn-item-up').addEventListener('click', moveItemUp, false);
  _('btn-item-down').addEventListener('click', moveItemDown, false);
  _('item-list').addEventListener('mouseup', selectItem, false);
  _('btn-tex-markdown').addEventListener('click', texToMarkdown, false);
  _('head-level-0').addEventListener('change', saveItem, false);
  _('head-level-1').addEventListener('change', saveItem, false);
  _('head-level-2').addEventListener('change', saveItem, false);
  _('head-text').addEventListener('change', saveItem, false);
  _('subhead-text').addEventListener('change', saveItem, false);
  var cmBody = CodeMirror(_('body-text-cell'), defaultCodeMirrorStyle);
  var cmSidebar = CodeMirror(_('sidebar-text-cell'), defaultCodeMirrorStyle);
  var cmMetadata = CodeMirror(_('metadata-text-cell'), defaultCodeMirrorStyle);

  if (NWSL_DATA.items.length > 0) {
    openItem(0);
  }

  function buildItemList() {
    var output = [], itemText;

    for (var i = 0; i < NWSL_DATA.items.length; i++) {
      output.push('<li id="item-' + i + '" class="hd-lvl-');
      output.push(NWSL_DATA.items[i].headLevel);
      if (i === selectedItem) {
        output.push(' hd-selected');
      }
      output.push('">');

      if (NWSL_DATA.items[i].headLevel === 0) {
        itemText = NWSL_DATA.items[i].bodyText.substr(0, 50);
      } else {
        itemText = NWSL_DATA.items[i].head;
      }

      if (itemText === '') {
        output.push('&nbsp;');
      } else {
        output.push(_.encodeForHtml(itemText));
      }

      output.push('</li>');
    }

    _('item-list').innerHTML = output.join('');
  }

  function saveItems() {
    var params = {};
    params.id = NWSL_DATA.id;
    params.lastModified = NWSL_DATA.lastModified;
    params.items = NWSL_DATA.items;
    Ajax.postJSON('../edit-rpc',
            params,
            function () {
              /* nothing */
            },
            'Saving…');
  }

  function doPreview() {
    window.open('../view/' + NWSL_DATA.year + '/' + NWSL_DATA.edition);
  }

  function selectItem(event) {
    var targetItem = parseInt(event.target.id.substr(5), 10);
    openItem(targetItem);
  }

  function openItem(item) {
    if (selectedItem !== -1) {
      _.removeClass(_('item-' + selectedItem), 'hd-selected');
    }

    if (item === -1) {
      if (NWSL_DATA.items.length > 0) {
        item = 0;
      }
    }

    if (item !== -1) {
      _('item-' + item).className = _('item-' + item).className + ' hd-selected';
      _('head-level-' + NWSL_DATA.items[item].headLevel).checked = true;
      _('head-text').value = NWSL_DATA.items[item].head;
      _('subhead-text').value = NWSL_DATA.items[item].subhead;
      _('nws-form').style.display = 'block';
      cmBody.off('change', saveItem);
      cmBody.setValue(NWSL_DATA.items[item].bodyText);
      cmBody.on('change', saveItem);
      cmSidebar.off('change', saveItem);
      cmSidebar.setValue(NWSL_DATA.items[item].sidebarText);
      cmSidebar.on('change', saveItem);
      cmMetadata.off('change', saveItem);
      cmMetadata.setValue(NWSL_DATA.items[item].metadataText);
      cmMetadata.on('change', saveItem);
      setTimeout(function () {
        cmBody.refresh();
        cmSidebar.refresh();
        cmMetadata.refresh();
      }, 0);
    } else {
      _('nws-form').style.display = 'none';
    }

    selectedItem = item;
  }

  function texToMarkdown() {
    _('head-text').value = NWSL_DATA.items[selectedItem].head =
            transformTeXToMarkdown(_('head-text').value);
    _('subhead-text').value = NWSL_DATA.items[selectedItem].subhead =
            transformTeXToMarkdown(_('subhead-text').value);
    cmBody.setValue(NWSL_DATA.items[selectedItem].bodyText =
            transformTeXToMarkdown(cmBody.getValue()));
    cmSidebar.setValue(NWSL_DATA.items[selectedItem].sidebarText =
            transformTeXToMarkdown(cmSidebar.getValue()));
    cmMetadata.setValue(NWSL_DATA.items[selectedItem].metadataText =
            transformTeXToMarkdown(cmMetadata.getValue()
                    // replace single returns with space, multiple returns with one newline
                    .replace(/ *([^↵\n])\n(?!\n)/g, '$1 ').replace(/\n\n+/g, '↵\n')
                    ));
  }

  function saveItem() {
    NWSL_DATA.items[selectedItem].headLevel =
            parseInt(document.querySelector('input[name="head-level"]:checked').value, 10);
    NWSL_DATA.items[selectedItem].head = _('head-text').value;
    NWSL_DATA.items[selectedItem].subhead = _('subhead-text').value;
    NWSL_DATA.items[selectedItem].bodyText = cmBody.getValue();
    NWSL_DATA.items[selectedItem].sidebarText = cmSidebar.getValue();
    NWSL_DATA.items[selectedItem].metadataText = cmMetadata.getValue();
    buildItemList();
  }

  function newItem() {
    // Insert a copy of an empty item after the currently selected one
    NWSL_DATA.items.splice(selectedItem + 1, 0, _.clone(emptyItem));
    // first, rebuild the item list, then open the new item
    buildItemList();
    openItem(selectedItem + 1);
  }

  function deleteItem() {
    NWSL_DATA.items.splice(selectedItem, 1);
    // first, open the item above the deleted one, then rebuild the item list
    openItem(selectedItem - 1);
    buildItemList();
  }

  function moveItemUp() {
    if (selectedItem > 0) {
      NWSL_DATA.items.move(selectedItem, selectedItem - 1);
      buildItemList();
      openItem(selectedItem - 1);
    }
  }

  function moveItemDown() {
    if (selectedItem < NWSL_DATA.items.length - 1) {
      NWSL_DATA.items.move(selectedItem, selectedItem + 1);
      buildItemList();
      openItem(selectedItem + 1);
    }
  }

  // Exposed functions
  return {
    destroyPane: function (callback) {
      callback();
    }
  };
})(this, this.document);
