/* global _, Pane */

(function () {
  var nav = [_('nav-newsletters'),
    _('nav-accounts'),
    _('nav-application'),
    _('logout')];

  for (var i = 0; i < nav.length; i++) {
    if (nav[i]) {
      nav[i].addEventListener('click', doNavigate, false);
    }
  }

  function doNavigate(e) {
    Pane.destroyPane(function () {
      location.href = e.target.href;
    });

    e.preventDefault();
  }
})();
