package nl.marcelkorpel.accounts.beans;

public class ErrorResponse {

  private int isLoggedOut;

  public int getIsLoggedOut() {
    return isLoggedOut;
  }

  public void setIsLoggedOut(int isLoggedOut) {
    this.isLoggedOut = isLoggedOut;
  }
}
