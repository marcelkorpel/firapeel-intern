package nl.marcelkorpel.accounts;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.marcelkorpel.application.model.ApplicationDefaults;
import nl.marcelkorpel.dao.ApplicationDefaultsDAO;
import nl.marcelkorpel.dao.DAOFactory;

/**
 *
 * @author mk
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

  private static final String SESSION_COOKIE = "SESSIONID";

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    ApplicationDefaultsDAO applicationDefaultsDAO = db.getApplicationDefaultsDAOJDBC();
    ApplicationDefaults applicationDefaults = applicationDefaultsDAO.getApplicationDefaults();
    String message = null;
    String username = null;
    HttpSession session = request.getSession(false);

    if (session != null) {
      message = (String) session.getAttribute("message");
      username = (String) session.getAttribute("username");
      session.invalidate();
    }

    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("application_name", applicationDefaults.getApplicationName());
    request.setAttribute("message", message);
    request.setAttribute("username", username);
    request.getRequestDispatcher("/WEB-INF/login.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String remember = request.getParameter("remember-me");

    if (!Domain.login(username, password, (remember != null), request, response)) {
      HttpSession session = request.getSession();
      session.setAttribute("message", "Username and/or password incorrect!");
      session.setAttribute("username", username);
    }

    response.setStatus(HttpServletResponse.SC_SEE_OTHER);
    response.setHeader("Location", request.getHeader("referer"));
  }
}
