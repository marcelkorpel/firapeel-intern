package nl.marcelkorpel.accounts;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static nl.marcelkorpel.accounts.Domain.setupAccount;
import nl.marcelkorpel.accounts.domain.AccountsError;
import nl.marcelkorpel.accounts.model.User;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.UserDAO;

@WebServlet("/passreset")
public class PasswordResetServlet extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    UserDAO userDAO = db.getUserDAOJDBC();
    String resetkey = request.getParameter("resetkey");
    User user = userDAO.getUserByResetKey(resetkey);

    if (user.getId() != null) {
      String message = null;
      String username = null;
      HttpSession session = request.getSession(false);

      if (session != null) {
        message = (String) session.getAttribute("message");
        username = (String) session.getAttribute("username");
        session.invalidate();
      }

      if (username == null) {
        username = user.getUsername();
      }

      response.setContentType("text/html; charset=UTF-8");
      request.setAttribute("resetkey", resetkey);
      request.setAttribute("message", message);
      request.setAttribute("username", username);
      request.getRequestDispatcher("/WEB-INF/passreset.xhtml").forward(request, response);
    } else {
      response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
    }
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    String resetKey = request.getParameter("reset-key");
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String password2 = request.getParameter("password2");

    if (!password.equals(password2)) {
      HttpSession session = request.getSession();
      session.setAttribute("message", "Passwords do not match");
      session.setAttribute("username", username);
      response.setStatus(HttpServletResponse.SC_SEE_OTHER);
      response.setHeader("Location", request.getHeader("referer"));
    } else if (username.length() > 255) {
      HttpSession session = request.getSession();
      session.setAttribute("message", "Username may not exceed 255 characters");
      session.setAttribute("username", username);
      response.setStatus(HttpServletResponse.SC_SEE_OTHER);
      response.setHeader("Location", request.getHeader("referer"));
    } else if (password.length() < 4 || password.length() > 512) {
      HttpSession session = request.getSession();
      session.setAttribute("message", "Password must be between 4 and 512 characters");
      session.setAttribute("username", username);
      response.setStatus(HttpServletResponse.SC_SEE_OTHER);
      response.setHeader("Location", request.getHeader("referer"));
    } else {
      try {
        setupAccount(username, password, resetKey, request, response);
        response.setStatus(HttpServletResponse.SC_SEE_OTHER);
        response.setHeader("Location", "app/");
      } catch (AccountsError e) {
        HttpSession session = request.getSession();
        session.setAttribute("message", e.getMessage());
        session.setAttribute("username", username);
        response.setStatus(HttpServletResponse.SC_SEE_OTHER);
        response.setHeader("Location", request.getHeader("referer"));
      }
    }
  }
}
