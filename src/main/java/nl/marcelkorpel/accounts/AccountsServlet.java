package nl.marcelkorpel.accounts;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.accounts.model.User;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.UserDAO;

/**
 *
 * @author mk
 */
@WebServlet("/app/accounts")
public class AccountsServlet extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    UserDAO userDAO = db.getUserDAOJDBC();
    List<User> users = userDAO.list();
    Gson gson = new Gson();
    String json = gson.toJson(users);

    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("data", json);
    request.getRequestDispatcher("/WEB-INF/accounts.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
