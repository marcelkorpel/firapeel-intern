package nl.marcelkorpel.accounts;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static nl.marcelkorpel.accounts.Domain.sendResetKey;

@WebServlet("/forgotpassword")
public class ForgotPasswordServlet extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html; charset=UTF-8");
    request.getRequestDispatcher("/WEB-INF/forgotpassword.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    sendResetKey(request.getParameter("email"), request);
    response.setStatus(HttpServletResponse.SC_SEE_OTHER);
    response.setHeader("Location", "forgotpassword-emailsubmitted.xhtml");
  }
}
