package nl.marcelkorpel.accounts;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.accounts.beans.ErrorResponse;

/**
 *
 * @author mk
 */
@WebFilter("/app/*")
public class AuthenticationFilter implements Filter {

  // The filter configuration object we are associated with.  If
  // this value is null, this filter instance is not currently
  // configured.
  private FilterConfig filterConfig = null;

  public AuthenticationFilter() {
  }

  /**
   *
   * @param request The servlet request we are processing
   * @param response The servlet response we are creating
   * @param chain The filter chain we are processing
   *
   * @exception IOException if an input/output error occurs
   * @exception ServletException if a servlet error occurs
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response,
          FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    String uri = req.getRequestURI();

    Throwable problem = null;

    try {
      // Fix UTF-8 encoding problem
      // http://balusc.omnifaces.org/2009/05/unicode-how-to-get-characters-right.html#JSPServletRequest
      if (request.getCharacterEncoding() == null) {
        request.setCharacterEncoding("UTF-8");
      }

      if (Domain.isLoggedIn(req)) {
        // http://stackoverflow.com/a/10305799/258127
        if (!uri.startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) { // Skip JSF resources (CSS/JS/Images/etc)
          res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
          res.setHeader("Pragma", "no-cache"); // HTTP 1.0
          res.setDateHeader("Expires", 0); // Proxies
        }

        chain.doFilter(request, response);
      } else if (uri.startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) {
        chain.doFilter(request, response);
      } else if (uri.endsWith("-rpc") || uri.endsWith("-upload")) {
        // Send back JSON string instead of login form
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setIsLoggedOut(1);
        Gson gson = new Gson();
        if (uri.endsWith("-rpc")) {
          response.setContentType("application/json;charset=UTF-8");
        } else {
          response.setContentType("text/html");  // Response to image upload iframe
        }
        response.getWriter().write(gson.toJson(errorResponse));
      } else {
        request.getRequestDispatcher("/login").forward(request, response);
      }
    } catch (IOException | ServletException t) {
      // If an exception is thrown somewhere down the filter chain,
      // we still want to execute our after processing, and then
      // rethrow the problem after that.
      problem = t;
    }

    // If there was a problem, we want to rethrow it if it is
    // a known type, otherwise log it.
    if (problem != null) {
      if (problem instanceof ServletException) {
        throw (ServletException) problem;
      }
      if (problem instanceof IOException) {
        throw (IOException) problem;
      }
      sendProcessingError(problem, response);
    }
  }

  /**
   * Return the filter configuration object for this filter.
   */
  public FilterConfig getFilterConfig() {
    return (this.filterConfig);
  }

  /**
   * Set the filter configuration object for this filter.
   *
   * @param filterConfig The filter configuration object
   */
  public void setFilterConfig(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  /**
   * Destroy method for this filter
   */
  @Override
  public void destroy() {
  }

  /**
   * Init method for this filter
   */
  @Override
  public void init(FilterConfig filterConfig) {
    this.filterConfig = filterConfig;
  }

  private void sendProcessingError(Throwable t, ServletResponse response) {
    String stackTrace = getStackTrace(t);

    if (stackTrace != null && !stackTrace.equals("")) {
      try {
        response.setContentType("text/html");
        PrintStream ps = new PrintStream(response.getOutputStream());
        PrintWriter pw = new PrintWriter(ps);
        pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

        // PENDING! Localize this for next official release
        pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
        pw.print(stackTrace);
        pw.print("</pre></body>\n</html>"); //NOI18N
        pw.close();
        ps.close();
        response.getOutputStream().close();
      } catch (Exception ex) {
      }
    } else {
      try {
        PrintStream ps = new PrintStream(response.getOutputStream());
        t.printStackTrace(ps);
        ps.close();
        response.getOutputStream().close();
      } catch (Exception ex) {
      }
    }
  }

  public static String getStackTrace(Throwable t) {
    String stackTrace = null;
    try {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      t.printStackTrace(pw);
      pw.close();
      sw.close();
      stackTrace = sw.getBuffer().toString();
    } catch (Exception ex) {
    }
    return stackTrace;
  }

  public void log(String msg) {
    filterConfig.getServletContext().log(msg);
  }
}
