package nl.marcelkorpel.accounts.model;

public class Session {

  // Properties
  private String id;
  private Long userId;
  private String provider;
  private String remoteHost;
  private String location;
  private Long updateTS;
  private Integer active;

  // Getters & Setters
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getRemoteHost() {
    return remoteHost;
  }

  public void setRemoteHost(String remoteHost) {
    this.remoteHost = remoteHost;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Long getUpdateTS() {
    return updateTS;
  }

  public void setUpdateTS(Long updateTS) {
    this.updateTS = updateTS;
  }

  public Integer getActive() {
    return active;
  }

  public void setActive(Integer active) {
    this.active = active;
  }
}
