package nl.marcelkorpel.accounts.util;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

public class Util {

  public static String createOrderedUUID() {
    // https://www.percona.com/blog/2014/12/19/store-uuid-optimized-way/
    EthernetAddress addr = EthernetAddress.fromInterface();
    TimeBasedGenerator uuidGenerator = Generators.timeBasedGenerator(addr);
    String uuid = uuidGenerator.generate().toString();

    return uuid.substring(14, 18) + uuid.substring(9, 13) + uuid.substring(0, 8)
            + uuid.substring(19, 23) + uuid.substring(24);
  }
}
