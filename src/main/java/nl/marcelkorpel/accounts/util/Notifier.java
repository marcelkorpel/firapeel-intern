package nl.marcelkorpel.accounts.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import nl.marcelkorpel.accounts.model.User;
import nl.marcelkorpel.application.model.ApplicationDefaults;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.dao.ApplicationDefaultsDAO;
import nl.marcelkorpel.dao.DAOFactory;

public class Notifier {

  private final Config cfg = new Config();
  private final String APP_NAME;

  public Notifier() {
    DAOFactory db = DAOFactory.getInstance();
    ApplicationDefaultsDAO applicationDefaultsDAO = db.getApplicationDefaultsDAOJDBC();
    ApplicationDefaults applicationDefaults = applicationDefaultsDAO.getApplicationDefaults();
    APP_NAME = applicationDefaults.getApplicationName();
  }

  public void notifyNewUser(User user, HttpServletRequest request)
          throws MessagingException, UnsupportedEncodingException {
    String to = user.getEmail();
    String subject = "Welcome!";
    String body = "Welcome to " + APP_NAME + ".\n\n"
            + "In order to set an initial username and password for your new "
            + "account, please click the following link:\n\n"
            + buildLocationURL(request)
            + "/passreset?resetkey=" + user.getResetKey() + "\n";

    sendNotification(to, subject, body);
  }

  public void sendResetKey(User user, HttpServletRequest request)
          throws MessagingException, UnsupportedEncodingException {
    String to = user.getEmail();
    String subject = "Password Reset";
    String body = "A password reset request was submitted for the account '"
            + user.getUsername()
            + "' associated with your email address. If you wish to reset your "
            + "password follow the link below, otherwise ignore this "
            + "message and nothing will happen.\n\n"
            + buildLocationURL(request)
            + "/passreset?resetkey=" + user.getResetKey() + "\n";

    sendNotification(to, subject, body);
  }

  private String buildLocationURL(HttpServletRequest request) {
    return request.getScheme() + "://"
            + request.getServerName()
            + ("http".equals(request.getScheme()) && request.getServerPort() == 80
            || "https".equals(request.getScheme()) && request.getServerPort() == 443
            ? "" : ":" + request.getServerPort())
            + request.getContextPath();
  }

  private void sendNotification(String to, String subject, String body)
          throws MessagingException, UnsupportedEncodingException {
    String protocol = cfg.getProperty("smtp_protocol", true);
    String hostname = cfg.getProperty("smtp_hostname", true);
    String starttls = cfg.getProperty("smtp_starttls", true);
    String port = cfg.getProperty("smtp_port", true);
    String username = cfg.getProperty("smtp_username", true);
    String password = cfg.getProperty("smtp_password", true);
    String from = cfg.getProperty("mail_from_address", true);

    Properties properties = new Properties();
    properties.put("mail.transport.protocol", protocol);
    properties.put("mail." + protocol + ".auth", "true");
    properties.put("mail." + protocol + ".host", hostname);
    properties.put("mail." + protocol + ".starttls", starttls);

    Session session = Session.getDefaultInstance(properties);

    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress(from, APP_NAME, "UTF-8"));
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
    message.setSubject(subject);
    message.setSentDate(new Date());
    // Wrap message after 72 chars
    // Note: body must end with \n
    body = body.replaceAll("(.{1,72})\\s", "$1\n");
    message.setText(body, "UTF-8");
    Transport transport = session.getTransport();
    transport.connect(hostname, Integer.parseInt(port), username, password);
    transport.sendMessage(message, message.getAllRecipients());
    transport.close();
  }
}
