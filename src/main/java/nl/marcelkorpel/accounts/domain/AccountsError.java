package nl.marcelkorpel.accounts.domain;

public class AccountsError extends RuntimeException {

  public AccountsError(String message) {
    super(message);
  }

  public AccountsError(Throwable cause) {
    super(cause);
  }

  public AccountsError(String message, Throwable cause) {
    super(message, cause);
  }
}
