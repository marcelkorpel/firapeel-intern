package nl.marcelkorpel.accounts.rpc;

import nl.marcelkorpel.accounts.model.User;

public class AccountsParams {

  private String action;
  private User user;

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
