package nl.marcelkorpel.accounts.rpc;

import java.util.List;
import nl.marcelkorpel.accounts.model.Session;
import nl.marcelkorpel.accounts.model.User;

public class AccountsResponse {

  private int ok;
  private String error;
  private String exception;
  private User user;
  private List<Session> sessions;

  public int getOk() {
    return ok;
  }

  public void setOk(int ok) {
    this.ok = ok;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getException() {
    return exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public List<Session> getSessions() {
    return sessions;
  }

  public void setSessions(List<Session> sessions) {
    this.sessions = sessions;
  }
}
