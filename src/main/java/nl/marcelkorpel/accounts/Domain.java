package nl.marcelkorpel.accounts;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.accounts.domain.AccountsError;
import nl.marcelkorpel.accounts.model.Session;
import nl.marcelkorpel.accounts.model.User;
import nl.marcelkorpel.accounts.util.Notifier;
import static nl.marcelkorpel.accounts.util.Util.createOrderedUUID;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.SessionDAO;
import nl.marcelkorpel.dao.UserDAO;

/**
 *
 * @author mk
 */
public class Domain {

  private static final String SESSION_COOKIE = "SESSIONID";
  private static final String DUMMY_HASH = "PBKDF2WithHmacSHA1:2000:"
          + "1895355931dc6751addc9d0b36a5dd00f02bb1c9f6ee81a60995fa54fbf9f241:"
          + "239230178822db689c2cd093e41a7d70fd8decbb3660397f2825ff590b5646c8";

  public static boolean login(String username, String password, boolean remember,
          HttpServletRequest request, HttpServletResponse response) {
    DAOFactory db = DAOFactory.getInstance();
    UserDAO userDAO = db.getUserDAOJDBC();
    User user = userDAO.getUserByUsername(username);

    if (password.length() <= 512) {
      if (user.getId() != null) {
        // Only password of initial user is saved as plain text
        if (user.getId() == 1) {
          if (password.equals(user.getPasswordHash())) {
            // Username and password match, create new session
            createSession(db, user, remember, request, response);
            return true;
          }
        } else {
          try {
            if (PasswordHash.validatePassword(password, user.getPasswordHash())) {
              // Username and password match, create new session
              createSession(db, user, remember, request, response);
              return true;
            }
          } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            // TODO: Log exception
          }
        }
      } else {
        try {
          // Always hash password to prevent timing attack
          PasswordHash.validatePassword(password, DUMMY_HASH);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
          // TODO: Log exception
        }
      }
    }

    return false;
  }

  public static Long getCurrentUserId(HttpServletRequest request) {
    String sessionId = getSessionCookieValue(request);
    DAOFactory db = DAOFactory.getInstance();
    SessionDAO sessionDAO = db.getSessionDAOJDBC();
    Session session = sessionDAO.getSession(sessionId);

    return session.getUserId();
  }

  public static boolean isLoggedIn(HttpServletRequest request) {
    return (getCurrentUserId(request) != null);
  }

  public static void logout(HttpServletRequest request, HttpServletResponse response) {
    String sessionId = getSessionCookieValue(request);
    DAOFactory db = DAOFactory.getInstance();
    SessionDAO sessionDAO = db.getSessionDAOJDBC();
    sessionDAO.inactivateSession(sessionId);

    // Immediately delete cookie
    Cookie cookie = new Cookie(SESSION_COOKIE, "");
    cookie.setMaxAge(0);
    cookie.setPath(request.getContextPath() + "/");
    cookie.setHttpOnly(true);
    response.addCookie(cookie);
  }

  public static void setupAccount(String username, String password, String resetKey,
          HttpServletRequest request, HttpServletResponse response)
          throws AccountsError {
    DAOFactory db = DAOFactory.getInstance();
    UserDAO userDAO = db.getUserDAOJDBC();
    User user = userDAO.getUserByResetKey(resetKey);
    user.setUsername(username);

    try {
      user.setPasswordHash(PasswordHash.createHash(password));
    } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
      // Re-throw exception
      // TODO: Don't re-throw as error, but as exception
      throw new AccountsError(ex);
    }

    userDAO.setUsernamePasswordHash(user);

    // Immediately login
    createSession(db, user, true, request, response);
  }

  public static void sendResetKey(String email, HttpServletRequest request) {
    DAOFactory db = DAOFactory.getInstance();
    UserDAO userDAO = db.getUserDAOJDBC();
    User user = userDAO.getUserByEmail(email);

    if (user.getId() != null) {
      user.setResetKey(createOrderedUUID());
      userDAO.setResetKey(user);
      Notifier notifier = new Notifier();
      try {
        notifier.sendResetKey(user, request);
      } catch (MessagingException | UnsupportedEncodingException ex) {
        // TODO: Log exception
      }
    }
  }

  private static void createSession(DAOFactory db, User user, boolean remember,
          HttpServletRequest request, HttpServletResponse response) {
    Session session = new Session();
    session.setId(createOrderedUUID());
    session.setUserId(user.getId());
    session.setRemoteHost(request.getRemoteHost());

    SessionDAO sessionDAO = db.getSessionDAOJDBC();
    sessionDAO.insertSession(session);
    Cookie cookie = new Cookie(SESSION_COOKIE, session.getId());

    if (remember) {
      cookie.setMaxAge(60 * 60 * 24 * 30 * 6);  // about 6 months
    }

    cookie.setPath(request.getContextPath() + "/");
    cookie.setHttpOnly(true);
    response.addCookie(cookie);
  }

  private static String getSessionCookieValue(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    String sessionId = null;

    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (SESSION_COOKIE.equals(cookie.getName())) {
          sessionId = cookie.getValue();
        }
      }
    }

    return sessionId;
  }
}
