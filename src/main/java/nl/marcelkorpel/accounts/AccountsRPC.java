package nl.marcelkorpel.accounts;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.accounts.domain.AccountsError;
import nl.marcelkorpel.accounts.util.Notifier;
import nl.marcelkorpel.accounts.model.User;
import nl.marcelkorpel.accounts.rpc.AccountsParams;
import nl.marcelkorpel.accounts.rpc.AccountsResponse;
import static nl.marcelkorpel.accounts.util.Util.createOrderedUUID;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.SessionDAO;
import nl.marcelkorpel.dao.UserDAO;

@WebServlet("/app/accounts-rpc")
public class AccountsRPC extends HttpServlet {

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    Gson gson = new Gson();
    AccountsResponse res = new AccountsResponse();
    response.setContentType("application/json;charset=UTF-8");

    try {
      switch (request.getParameter("action")) {
        case "retrieve-sessions":
          DAOFactory db = DAOFactory.getInstance();
          SessionDAO sessionDAO = db.getSessionDAOJDBC();
          long userId = Long.parseLong(request.getParameter("userId"));
          res.setSessions(sessionDAO.getSessions(userId));
          break;
        default:
          res.setException("Illegal RPC operation");
      }
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    Gson gson = new Gson();
    String json = request.getParameter("data");
    AccountsParams params = gson.fromJson(json, AccountsParams.class);
    AccountsResponse res = new AccountsResponse();
    response.setContentType("application/json;charset=UTF-8");

    try {
      DAOFactory db = DAOFactory.getInstance();
      UserDAO userDAO = db.getUserDAOJDBC();
      User user = params.getUser();

      switch (params.getAction()) {
        case "new":
          user.setResetKey(createOrderedUUID());
          userDAO.newUser(user);
          Notifier notifier = new Notifier();
          notifier.notifyNewUser(user, request);
          res.setUser(user);
          res.setOk(1);
          break;
        case "save":
          userDAO.updateUser(user);
          res.setUser(user);
          res.setOk(1);
          break;
        case "delete":
          userDAO.inactivateUser(user);
          SessionDAO sessionDAO = db.getSessionDAOJDBC();
          sessionDAO.inactivateSessions(user);
          res.setOk(1);
          break;
        default:
          res.setException("Illegal RPC operation");
      }
    } catch (DAOException | MessagingException | UnsupportedEncodingException e) {
      res.setException(e.getMessage());
    } catch (AccountsError e) {
      res.setError(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
