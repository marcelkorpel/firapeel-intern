package nl.marcelkorpel.newsletter.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.ImageDAO;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.model.NewsletterItem;

public class ImageExtractor {

  // Constant
  // Matching-quotes RegEx from http://www.metaltoad.com/blog/regex-quoted-string-escapable-quotes
  public static final Pattern IMAGE_PARSER
          = Pattern.compile("!\\[(.*)\\]\\((\\d+)(?:\\s*((?<![\\\\])['\"])((?:.(?!(?<![\\\\])\\3))*.?)\\3)?\\)");

  // Public methods
  public static Map<Long, Image> retrieveImageMap(DefaultText defaultText,
          List<NewsletterItem> items) {
    return retrieveImageMap(defaultText, items, "");
  }

  public static Map<Long, Image> retrieveImageMap(DefaultText defaultText,
          List<NewsletterItem> items, String extraText) throws DAOException {
    Map<Long, Image> imageMap = new HashMap<>();
    DAOFactory db = DAOFactory.getInstance();
    ImageDAO imageDAO = db.getImageDAO();

    Set<Long> ids = extractImageIds(defaultText, items, extraText);

    if (!ids.isEmpty()) {
      imageMap = imageDAO.retrieveImageMap(ids);
      Set<Long> imgIds = extractImageIds(imageMap);
      // Subtract original set from new set: only retrieve currently
      // unavailable images
      imgIds.removeAll(ids);

      if (!imgIds.isEmpty()) {
        imageMap.putAll(imageDAO.retrieveImageMap(imgIds));
      }
    }

    return imageMap;
  }

  // Private methods
  private static Set<Long> extractImageIds(DefaultText defaultText,
          List<NewsletterItem> items, String extraText) {
    Set<Long> ids = new HashSet<>();

    ids.addAll(processString(defaultText.getHtmlOnline()));
    ids.addAll(processString(defaultText.getHeaderText()));
    ids.addAll(processString(defaultText.getColophonHead()));
    ids.addAll(processString(defaultText.getColophonText()));
    ids.addAll(processString(defaultText.getFooterText()));

    for (NewsletterItem item : items) {
      ids.addAll(processString(item.getHead()));
      ids.addAll(processString(item.getSubhead()));
      ids.addAll(processString(item.getBodyText()));
      ids.addAll(processString(item.getSidebarText()));
      ids.addAll(processString(item.getMetadataText()));
    }

    ids.addAll(processString(extraText));

    return ids;
  }

  private static Set<Long> extractImageIds(Map<Long, Image> imageMap) {
    Set<Long> ids = new HashSet<>();

    for (Image image : imageMap.values()) {
      ids.addAll(processString(image.getDescription()));
    }

    return ids;
  }

  private static Set<Long> processString(String markdownText) {
    // NOTE: only inline image syntax is supported!
    Set<Long> ids = new HashSet<>();
    Matcher m = IMAGE_PARSER.matcher(markdownText);

    while (m.find()) {
      ids.add(Long.parseLong(m.group(2)));
    }

    return ids;
  }
}
