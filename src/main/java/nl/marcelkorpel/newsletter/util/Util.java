package nl.marcelkorpel.newsletter.util;

import java.util.Map;
import nl.marcelkorpel.newsletter.model.Image;

public class Util {

  public static long calculateMaxTimestamp(Map<Long, Image> imageMap) {
    long max = 0;

    for (Image image : imageMap.values()) {
      max = Math.max(image.getUpdateTS(), max);
    }

    return max;
  }
}
