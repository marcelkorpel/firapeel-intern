package nl.marcelkorpel.newsletter.deployment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.ImageDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.model.Newsletter;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class Deployer {

  // Constants
  private static final Config CONFIG = new Config();
  private static final String UPLOAD_PATH = Config.APPROOT + Config.UPLOAD_DIR + "/newsletter/image";
  private static final String DEPLOYMENT_URL = CONFIG.getProperty("deployment_url", true);
  private static final String UPLOAD_IMAGES = "/upload-images.php";
  private static final String UPLOAD_HTML = "/upload-html.php";

  public static void deployImages(Map<Long, Image> imageMap) throws DAOException {

    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      HttpPost httpPost;

      // Create a custom response handler
      ResponseHandler<String> responseHandler = (final HttpResponse response) -> {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
          HttpEntity entity = response.getEntity();
          return entity != null ? EntityUtils.toString(entity) : null;
        } else {
          throw new ClientProtocolException("Unexpected response status: " + status);
        }
      };

      httpPost = new HttpPost(DEPLOYMENT_URL + UPLOAD_IMAGES);
      MultipartEntityBuilder entity = MultipartEntityBuilder.create();

      FileBody fileBody;
      List<Long> deployedImageIds = new LinkedList<>();

      for (Image image : imageMap.values()) {
        if (image.getDeployedFilename() == null) {
          fileBody = new FileBody(new File(UPLOAD_PATH + "/"
                  + image.getLocalFilename()));
          entity.addPart("file[]", fileBody);
          deployedImageIds.add(image.getId());
        }
      }

      if (!deployedImageIds.isEmpty()) {
        httpPost.setEntity(entity.build());
        String response = httpClient.execute(httpPost, responseHandler);

        Type listType = new TypeToken<List<String>>() {
        }.getType();
        Gson gson = new Gson();
        List<String> deployedFilenames = gson.fromJson(response, listType);

        for (int i = 0; i < deployedImageIds.size(); i++) {
          Image image = imageMap.get(deployedImageIds.get(i));
          image.setDeployedFilename(deployedFilenames.get(i));
          imageMap.replace(deployedImageIds.get(i), image);
        }

        DAOFactory db = DAOFactory.getInstance();
        ImageDAO imageDAO = db.getImageDAO();
        imageDAO.updateDeployedFilenames(imageMap);
      }
    } catch (IOException e) {

    } catch (DAOException e) {
      throw new DAOException(e.getMessage());
    }
  }

  public static void deployHTML(Newsletter newsletter, String html) throws DAOException {

    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      HttpPost httpPost;

      // Create a custom response handler
      ResponseHandler<String> responseHandler = (final HttpResponse response) -> {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
          HttpEntity entity = response.getEntity();
          return entity != null ? EntityUtils.toString(entity) : null;
        } else {
          throw new ClientProtocolException("Unexpected response status: " + status);
        }
      };

      httpPost = new HttpPost(DEPLOYMENT_URL + UPLOAD_HTML);
      List<NameValuePair> params = new LinkedList<>();
      params.add(new BasicNameValuePair("html", html));
      httpPost.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));
      String response = httpClient.execute(httpPost, responseHandler);

      Gson gson = new Gson();
      newsletter.setDeployedFilename(gson.fromJson(response, String.class));

      DAOFactory db = DAOFactory.getInstance();
      NewsletterDAO newsletterDAO = db.getNewsletterDAO();
      newsletterDAO.update(newsletter);
    } catch (IOException e) {

    } catch (DAOException e) {
      throw new DAOException(e.getMessage());
    }
  }
}
