/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.rpc.DefaultPostResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/defaulttexts-rpc")
public class DefaultTextsRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DefaultPostResponse res = new DefaultPostResponse();
    Gson gson = new Gson();
    response.setContentType("application/json;charset=UTF-8");

    try {
      DAOFactory db = DAOFactory.getInstance();
      DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
      DefaultText oldDefaultText = defaultTextDAO.getDefaultText();
      String json = request.getParameter("data");
      DefaultText newDefaultText = gson.fromJson(json, DefaultText.class);
      defaultTextDAO.setDefaultText(newDefaultText);
      // Has one of the default newsletter parts been changed?
      if (!oldDefaultText.getHeaderText().equals(newDefaultText.getHeaderText())
              || !oldDefaultText.getColophonHead().equals(newDefaultText.getColophonHead())
              || !oldDefaultText.getColophonText().equals(newDefaultText.getColophonText())
              || !oldDefaultText.getFooterText().equals(newDefaultText.getFooterText())) {
        NewsletterDAO newsletterDAO = db.getNewsletterDAO();
        newsletterDAO.removeDeployedFilenames();
      }
      res.setOk(1);
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
