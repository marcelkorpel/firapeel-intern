package nl.marcelkorpel.newsletter;

import nl.marcelkorpel.newsletter.domain.Parser;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.dao.NewsletterItemDAO;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItem;
import static nl.marcelkorpel.newsletter.util.ImageExtractor.retrieveImageMap;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/view/*")
public class Preview extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
    NewsletterDAO newsletterDAO = db.getNewsletterDAO();
    NewsletterItemDAO newsletterItemDAO = db.getNewsletterItemDAO();
    DefaultText defaultText = defaultTextDAO.getDefaultText();
    String params = request.getPathInfo();
    int year = Integer.parseInt(params.substring(1, params.lastIndexOf("/")));
    String edition = params.substring(params.lastIndexOf("/") + 1);
    Newsletter newsletter = newsletterDAO.getNewsletter(year, edition);
    List<NewsletterItem> items = newsletterItemDAO.getItems(newsletter.getId());
    Map imageMap = retrieveImageMap(defaultText, items);

    Parser parser = new Parser(defaultText, newsletter, items, imageMap, true);

    /*
    String text = parser.createNewsletterPlainText(null, false);
    response.setContentType("text/plain; charset=UTF-8");
    response.getWriter().write(text);
    */
    String html = parser.createNewsletterHTML();
    response.setContentType("text/html; charset=UTF-8");
    response.getWriter().write(html);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
