package nl.marcelkorpel.newsletter.model;

/**
 *
 * @author mk
 */
public class Image {

  // Properties
  private Long id;
  private String name;
  private String localFilename;
  private String deployedFilename;
  private Integer width;
  private Integer height;
  private String altText;
  private String titleText;
  private String description;
  private Long updateTS;

  // Getters and setters
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocalFilename() {
    return localFilename;
  }

  public void setLocalFilename(String localFilename) {
    this.localFilename = localFilename;
  }

  public String getDeployedFilename() {
    return deployedFilename;
  }

  public void setDeployedFilename(String deployedFilename) {
    this.deployedFilename = deployedFilename;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public String getAltText() {
    return altText;
  }

  public void setAltText(String altText) {
    this.altText = altText;
  }

  public String getTitleText() {
    return titleText;
  }

  public void setTitleText(String titleText) {
    this.titleText = titleText;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getUpdateTS() {
    return updateTS;
  }

  public void setUpdateTS(Long updateTS) {
    this.updateTS = updateTS;
  }
}
