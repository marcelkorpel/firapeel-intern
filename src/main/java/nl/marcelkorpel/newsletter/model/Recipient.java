package nl.marcelkorpel.newsletter.model;

import java.io.Serializable;

/**
 *
 * @author mk
 */
public class Recipient implements Serializable {

  // Properties
  Long id;
  String address;

  // Getters/setters
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return address;
  }
}
