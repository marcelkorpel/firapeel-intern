package nl.marcelkorpel.newsletter.model;

import java.io.Serializable;

/**
 *
 * @author mk
 */
public class NewsletterItem implements Serializable {

  // Constants
  private static final long serialVersionUID = 1L;

  // Properties
  private Long id;
  private Integer headLevel;
  private String head;
  private String subhead;
  private String bodyText;
  private String sidebarText;
  private String metadataText;

  // Getters/setters
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getHeadLevel() {
    return headLevel;
  }

  public void setHeadLevel(Integer headLevel) {
    this.headLevel = headLevel;
  }

  public String getHead() {
    return head;
  }

  public void setHead(String head) {
    this.head = head;
  }

  public String getSubhead() {
    return subhead;
  }

  public void setSubhead(String subhead) {
    this.subhead = subhead;
  }

  public String getBodyText() {
    return bodyText;
  }

  public void setBodyText(String bodyText) {
    this.bodyText = bodyText;
  }

  public String getSidebarText() {
    return sidebarText;
  }

  public void setSidebarText(String sidebarText) {
    this.sidebarText = sidebarText;
  }

  public String getMetadataText() {
    return metadataText;
  }

  public void setMetadataText(String metadataText) {
    this.metadataText = metadataText;
  }

  // Object overrides
  /**
   * The newsletter ID is unique for each Newsletter. So this should compare
   * Newsletter by ID only.
   *
   * @param other Other object to compare with
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object other) {
    return (other instanceof NewsletterItem) && (id != null)
            ? id.equals(((NewsletterItem) other).id)
            : (other == this);
  }

  /**
   * The newsletter ID is unique for each Newsletter. So Newsletter with same ID
   * should return same hashcode.
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return (id != null)
            ? (this.getClass().hashCode() + id.hashCode())
            : super.hashCode();
  }

  /**
   * Returns the String representation of this Newsletter. Not required, it just
   * pleases reading logs.
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return String.format("NewsletterItem[id=%d,head=%s]",
            id, head);
  }
}
