package nl.marcelkorpel.newsletter.model;

import java.util.List;

public class SendMailData {

  // Properties
  private List<Newsletter> newsletters;
  private List<Recipient> recipients;
  private String welcomeText;

  // Getters and setters
  public List<Newsletter> getNewsletters() {
    return newsletters;
  }

  public void setNewsletters(List<Newsletter> newsletters) {
    this.newsletters = newsletters;
  }

  public List<Recipient> getRecipients() {
    return recipients;
  }

  public void setRecipients(List<Recipient> recipients) {
    this.recipients = recipients;
  }

  public String getWelcomeText() {
    return welcomeText;
  }

  public void setWelcomeText(String welcomeText) {
    this.welcomeText = welcomeText;
  }
}
