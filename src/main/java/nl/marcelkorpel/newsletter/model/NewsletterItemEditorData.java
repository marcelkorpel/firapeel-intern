/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter.model;

import java.util.List;

/**
 *
 * @author mk
 */
public class NewsletterItemEditorData {

  // Properties
  private long id;
  private int year;
  private String edition;
  private long updateTS;
  List<NewsletterItem> items;

  // Getters and setters
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public String getEdition() {
    return edition;
  }

  public void setEdition(String edition) {
    this.edition = edition;
  }

  public long getUpdateTS() {
    return updateTS;
  }

  public void setUpdateTS(long updateTS) {
    this.updateTS = updateTS;
  }

  public List<NewsletterItem> getItems() {
    return items;
  }

  public void setItems(List<NewsletterItem> items) {
    this.items = items;
  }
}
