package nl.marcelkorpel.newsletter.model;

import java.io.Serializable;

/**
 *
 * @author mk
 */
public class DefaultText implements Serializable {

  // Constants
  private static final long serialVersionUID = 1L;

  // Properties
  private String fromName;
  private String replyToAddress;
  private String subject;
  private String plainTextOnline;
  private String htmlOnline;
  private String headerText;
  private String colophonHead;
  private String colophonText;
  private String footerText;
  private String welcomeText;

  // Getters/setters
  public String getFromName() {
    return fromName;
  }

  public void setFromName(String fromName) {
    this.fromName = fromName;
  }

  public String getReplyToAddress() {
    return replyToAddress;
  }

  public void setReplyToAddress(String replyToAddress) {
    this.replyToAddress = replyToAddress;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getPlainTextOnline() {
    return plainTextOnline;
  }

  public void setPlainTextOnline(String plainTextOnline) {
    this.plainTextOnline = plainTextOnline;
  }

  public String getHtmlOnline() {
    return htmlOnline;
  }

  public void setHtmlOnline(String htmlOnline) {
    this.htmlOnline = htmlOnline;
  }

  public String getHeaderText() {
    return headerText;
  }

  public void setHeaderText(String headerText) {
    this.headerText = headerText;
  }

  public String getColophonHead() {
    return colophonHead;
  }

  public void setColophonHead(String colophonHead) {
    this.colophonHead = colophonHead;
  }

  public String getColophonText() {
    return colophonText;
  }

  public void setColophonText(String colophonText) {
    this.colophonText = colophonText;
  }

  public String getFooterText() {
    return footerText;
  }

  public void setFooterText(String footerText) {
    this.footerText = footerText;
  }

  public String getWelcomeText() {
    return welcomeText;
  }

  public void setWelcomeText(String welcomeText) {
    this.welcomeText = welcomeText;
  }
}
