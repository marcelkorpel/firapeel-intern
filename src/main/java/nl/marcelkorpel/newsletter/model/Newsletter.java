package nl.marcelkorpel.newsletter.model;

import java.io.Serializable;

/**
 *
 * @author mk
 */
public class Newsletter implements Serializable {

  // Constants
  private static final long serialVersionUID = 1L;

  // Properties
  private Long id;
  private Long userId;
  private String deployedFilename;
  private Integer year;
  private String edition;
  private String subject;
  private Long updateTS;

  // Getters/setters
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getDeployedFilename() {
    return deployedFilename;
  }

  public void setDeployedFilename(String deployedFilename) {
    this.deployedFilename = deployedFilename;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public String getEdition() {
    return edition;
  }

  public void setEdition(String edition) {
    this.edition = edition;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Long getUpdateTS() {
    return updateTS;
  }

  public void setUpdateTS(Long updateTS) {
    this.updateTS = updateTS;
  }

  // Object overrides
  /**
   * The newsletter ID is unique for each Newsletter. So this should compare
   * Newsletter by ID only.
   *
   * @param other Other object to compare with
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object other) {
    return (other instanceof Newsletter) && (id != null)
            ? id.equals(((Newsletter) other).id)
            : (other == this);
  }

  /**
   * The newsletter ID is unique for each Newsletter. So Newsletter with same ID
   * should return same hashcode.
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return (id != null)
            ? (this.getClass().hashCode() + id.hashCode())
            : super.hashCode();
  }

  /**
   * Returns the String representation of this Newsletter. Not required, it just
   * pleases reading logs.
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return String.format("Newsletter[id=%d,year=%d,edition=%s,subject=%s]",
            id, year, edition, subject);
  }
}
