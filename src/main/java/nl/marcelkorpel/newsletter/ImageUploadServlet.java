/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.ImageDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.rpc.ImageUploadResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/image-upload")
@MultipartConfig
public class ImageUploadServlet extends HttpServlet {

  // Constants
  private static final String UPLOAD_PATH;
  private static final File UPLOAD_DIR;
  private static final String PREFIX = "image";

  static {
    UPLOAD_PATH = Config.APPROOT + Config.UPLOAD_DIR + "/newsletter/image";

    // Create directory in case it doesn't exist
    UPLOAD_DIR = new File(UPLOAD_PATH);
    UPLOAD_DIR.mkdirs();
  }

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    ImageUploadResponse res = new ImageUploadResponse();
    Image image = new Image();
    Gson gson = new Gson();
    long id = Long.parseLong(request.getParameter("image-id"));

    Part part = request.getPart("file");
    String fileName = part.getSubmittedFileName();
    String suffix = "";

    if (fileName.contains(".")) {
      suffix = fileName.substring(fileName.lastIndexOf('.')).toLowerCase();
    }

    File file = File.createTempFile(PREFIX + "_", suffix, UPLOAD_DIR);
    try (InputStream inputStream = part.getInputStream()) {
      Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

      DAOFactory db = DAOFactory.getInstance();
      NewsletterDAO newsletterDAO = db.getNewsletterDAO();
      Newsletter lastNewsletter = newsletterDAO.getLast();
      Integer year = lastNewsletter.getYear();
      String prefix = "";

      if (year != null) {
        prefix = year + "/" + lastNewsletter.getEdition() + ": ";
      }

      image.setName(prefix + part.getSubmittedFileName());
      image.setLocalFilename(file.getName());
      BufferedImage bimg = ImageIO.read(file);

      if (bimg == null) {
        throw new IOException("Unsupported file format!");
      }

      image.setWidth(bimg.getWidth());
      image.setHeight(bimg.getHeight());

      ImageDAO imageDAO = db.getImageDAO();

      if (id == 0) {
        imageDAO.createAfterUpload(image);
      } else {
        image.setId(id);
        imageDAO.updateAfterUpload(image);
      }

      res.setOk(1);
      res.setImage(image);
    } catch (DAOException | IOException e) {
      file.delete();
      res.setException(e.getMessage());
    } finally {
      response.setContentType("text/html");  // DON'T send as application/json
      response.getWriter().write(gson.toJson(res));
    }
  }
}
