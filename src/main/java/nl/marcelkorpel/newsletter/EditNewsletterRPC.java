/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static nl.marcelkorpel.accounts.Domain.getCurrentUserId;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.dao.NewsletterItemDAO;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItemEditorData;
import nl.marcelkorpel.newsletter.rpc.DefaultPostResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/edit-rpc")
public class EditNewsletterRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DefaultPostResponse res = new DefaultPostResponse();
    Gson gson = new Gson();
    response.setContentType("application/json;charset=UTF-8");

    try {
      DAOFactory db = DAOFactory.getInstance();
      NewsletterItemDAO newsletterItemDAO = db.getNewsletterItemDAO();
      NewsletterDAO newsletterDAO = db.getNewsletterDAO();

      String json = request.getParameter("data");
      NewsletterItemEditorData data = gson.fromJson(json, NewsletterItemEditorData.class);
      newsletterItemDAO.setItems(data.getId(), data.getItems());

      // Update user ID and timestamp of newsletter
      Newsletter newsletter = newsletterDAO.getNewsletter(data.getId());
      newsletter.setUserId(getCurrentUserId(request));
      newsletterDAO.update(newsletter);

      res.setOk(1);
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
