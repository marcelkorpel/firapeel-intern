/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.ImageDAO;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.rpc.DefaultPostResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/images-rpc")
public class ImagesRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    Gson gson = new Gson();
    String json = request.getParameter("data");
    Image image = gson.fromJson(json, Image.class);
    DefaultPostResponse res = new DefaultPostResponse();

    try {
      DAOFactory db = DAOFactory.getInstance();
      ImageDAO imageDAO = db.getImageDAO();

      if (image.getId() == 0) {
        image.setId(null);
        imageDAO.createAfterSave(image);
      } else {
        imageDAO.updateAfterSave(image);
      }

      res.setOk(1);
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.setContentType("application/json;charset=UTF-8");
      response.getWriter().write(gson.toJson(res));
    }
  }
}
