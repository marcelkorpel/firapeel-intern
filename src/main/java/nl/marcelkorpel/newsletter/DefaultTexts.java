/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.newsletter.model.DefaultText;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/defaulttexts")
public class DefaultTexts extends HttpServlet {

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
    DefaultText defaultText = defaultTextDAO.getDefaultText();
    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("from_name", defaultText.getFromName());
    request.setAttribute("reply_to", defaultText.getReplyToAddress());
    request.setAttribute("subject", defaultText.getSubject());
    request.setAttribute("plain_text_online", defaultText.getPlainTextOnline());
    request.setAttribute("html_online", defaultText.getHtmlOnline());
    request.setAttribute("header_text", defaultText.getHeaderText());
    request.setAttribute("colophon_head", defaultText.getColophonHead());
    request.setAttribute("colophon_text", defaultText.getColophonText());
    request.setAttribute("footer_text", defaultText.getFooterText());
    request.setAttribute("welcome_text", defaultText.getWelcomeText());
    request.getRequestDispatcher("/WEB-INF/newsletter-defaulttext.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
