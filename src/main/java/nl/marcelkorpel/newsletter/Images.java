/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.ImageDAO;
import nl.marcelkorpel.newsletter.model.Image;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/images")
public class Images extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    ImageDAO imageDAO = db.getImageDAO();
    List<Image> images = imageDAO.list();
    Gson gson = new Gson();
    String json = gson.toJson(images);
    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("data", json);
    request.getRequestDispatcher("/WEB-INF/newsletter-images.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
