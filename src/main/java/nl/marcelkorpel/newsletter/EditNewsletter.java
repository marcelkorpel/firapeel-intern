/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.dao.NewsletterItemDAO;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItemEditorData;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/edit/*")
public class EditNewsletter extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    NewsletterDAO newsletterDAO = db.getNewsletterDAO();
    NewsletterItemDAO newsletterItemDAO = db.getNewsletterItemDAO();
    String id = request.getPathInfo().substring(1);
    long newsletterId = Long.parseLong(id);
    Newsletter newsletter = newsletterDAO.getNewsletter(newsletterId);
    NewsletterItemEditorData data = new NewsletterItemEditorData();

    data.setId(newsletterId);
    data.setYear(newsletter.getYear());
    data.setEdition(newsletter.getEdition());
    data.setUpdateTS(newsletter.getUpdateTS());
    data.setItems(newsletterItemDAO.getItems(newsletterId));

    Gson gson = new Gson();
    String json = gson.toJson(data);
    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("nwsl_data", json);
    request.getRequestDispatcher("/WEB-INF/newsletter-edit.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
