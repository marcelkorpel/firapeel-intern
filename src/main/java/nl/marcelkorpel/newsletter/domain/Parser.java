package nl.marcelkorpel.newsletter.domain;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItem;
import nl.marcelkorpel.newsletter.util.ImageExtractor;
import org.owasp.encoder.Encode;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

/**
 *
 * @author mk
 */
public class Parser {

  private final PegDownProcessor PEGDOWN_NORMAL
          = new PegDownProcessor(Extensions.TABLES
                  | Extensions.SMARTYPANTS
                  | Extensions.AUTOLINKS);
  private final PegDownProcessor PEGDOWN_HARDWRAPS
          = new PegDownProcessor(Extensions.HARDWRAPS
                  | Extensions.TABLES
                  | Extensions.SMARTYPANTS
                  | Extensions.AUTOLINKS);
  // RegEx based on http://www.metaltoad.com/blog/regex-quoted-string-escapable-quotes
  private final Pattern LINK_PARSER = Pattern
          .compile("\\[(.+)\\]\\(((.(?![\"']))+)(?:\\s+([\"'])((?:.(?!(?<![\\\\])\\4))*.?)\\4)?\\)");
  //         at the moment, there MUST be a space  ^^^^ between link and quote character
  private final Config cfg = new Config();
  private final String deploymentUrl = cfg.getProperty("deployment_url", true);
  private final DefaultText defaultText;
  private final Newsletter newsletter;
  private final List<NewsletterItem> items;
  private final Map<Long, Image> imageMap;
  private final boolean local;

  // Constructor
  public Parser(DefaultText defaultText, Newsletter newsletter,
          List<NewsletterItem> items, Map<Long, Image> imageMap, boolean local) {
    this.defaultText = defaultText;
    this.newsletter = newsletter;
    this.items = items;
    this.imageMap = imageMap;
    this.local = local;
  }

  // Public functions
  public String createNewsletterPlainText(String welcomeText, boolean includeViewOnline) {
    String text = "", sidebarText, metadataText;

    if (welcomeText != null) {
      text += mdToPlainText(welcomeText) + "\n\n";
      text += "------------------------------------------------------------------------\n\n";
    }

    if (includeViewOnline && !"".equals(defaultText.getPlainTextOnline())) {
      text += defaultText.getPlainTextOnline()
              .replace("%u", deploymentUrl + "/mail/" + newsletter.getDeployedFilename())
              + "\n\n";
      text += "------------------------------------------------------------------------\n\n";
    }

    for (NewsletterItem item : items) {
      switch (item.getHeadLevel()) {
        case 1:
          text += mdToPlainText(item.getHead()).toUpperCase() + "\n";
          break;
        case 2:
          text += mdToPlainText(item.getHead());
          if (!item.getSubhead().equals("")) {
            text += mdToPlainText(item.getSubhead());
          }
          text += "\n";
        // intended fall-through
        default:
          text += mdToPlainText(item.getBodyText()) + "\n";
          sidebarText = mdToPlainText(item.getSidebarText()).trim();
          if (!sidebarText.equals("")) {
            text += sidebarText + "\n";
          }
          metadataText = mdToPlainText(item.getMetadataText()).trim();
          if (!metadataText.equals("")) {
            text += metadataText + "\n\n";
          }
          text += "------------------------------------------------------------------------\n\n";
      }
    }

    text += mdToPlainText(defaultText.getColophonHead()).toUpperCase() + "\n";
    text += mdToPlainText(defaultText.getColophonText()) + "\n";
    text += "------------------------------------------------------------------------\n\n";
    text += mdToPlainText(defaultText.getFooterText());
    // Below line is no longer needed, as a trailing newline character is
    // automatically inserted by mdToPlainText
    //text += "\n"; // Needed to handle last line correctly
    text = text.replaceAll("(.{1,72})\\s", "$1\n");
    return text;
  }

  public String createNewsletterHTML() {
    return createNewsletterHTML(null, false);
  }

  public String createNewsletterHTML(String welcomeText, boolean includeViewOnline) {
    String html = "";
    String title = newsletter.getSubject()
            .replace("%y", newsletter.getYear().toString())
            .replace("%e", newsletter.getEdition());
    html += head(title);
    html += "<body>";

    if (welcomeText != null) {
      html += welcomeMessage(mdHardwraps(welcomeText));
    }

    if (includeViewOnline && !"".equals(defaultText.getHtmlOnline())) {
      html += viewOnline(mdNormal(defaultText.getHtmlOnline()));
    }

    html += headerOrFooter(mdNormal(defaultText.getHeaderText()));

    for (NewsletterItem item : items) {
      switch (item.getHeadLevel()) {
        case 1:
          html += headLevel1(mdNormal(item.getHead()));
          break;
        case 2:
          html += headLevel2(mdNormal(item.getHead()), mdItalicized(item.getSubhead()));
        // intended fall-through
        default:
          html += contentRow(
                  mdNormal(item.getBodyText()),
                  mdNormal(item.getSidebarText()));
          if (!item.getMetadataText().equals("")) {
            html += contentRow(mdNormal(item.getMetadataText()), "");
          }
          html += rulerRow();
      }
    }

    html += headLevel1(mdNormal(defaultText.getColophonHead()));
    html += contentRow(mdNormal(defaultText.getColophonText()), "");
    html += headerOrFooter(mdNormal(defaultText.getFooterText()));
    html += "</body>";
    html += "</html>";
    return html;
  }

  // Private functions
  private String mdToPlainText(String text) {
    text = text.replace("***", "")
            .replace("**", "")
            .replace("\\*", "ßß")
            .replaceAll("(\\S)\\*(?!↵)(\\S)", "$1ßß$2")
            .replaceAll("(\\s)\\*(\\s)", "$1ßß$2")
            .replace("*", "")
            .replace("ßß", "*")
            .replace("␣", "\u00a0")
            .replace("&nbsp;", "\u00a0")
            //.replaceAll("↵\n?", "\n")
            .replaceAll("\\r?\\n", "\n")
            .replaceAll("!\\[.*\\]\\(.*\\)\\n?", "")
            .replaceAll("\\[(.*)\\]\\(.*\\)", "$1")
            .replaceAll("\\n\\n+", "\r")
            .replaceAll("\\s*\\n", " ")
            .replace("\r", "\n\n")
            .replaceAll("↵ ?", "\n")
            .replaceAll("\\n+$", "");
    if (!text.equals("")) {
      text += "\n";
    }
    return text;
  }

  private String mdNormal(String text) {
    return mdToHtml(text, false);
  }

  private String mdHardwraps(String text) {
    return mdToHtml(text, true);
  }

  private String mdItalicized(String text) {
    if (text != null && !text.equals("")) {
      String html = "<span style=\"font-style: italic\">";
      html += mdNormal(text)
              .replace("<i>", "<span style=\"font-style: normal\">")
              .replace("</i>", "</span>");
      html += "</span>";
      return html;
    } else {
      return "";
    }
  }

  private String mdToHtml(String text, boolean hardwraps) {
    String html;

    // Parse images containing IDs as links
    text = parseImages(text);

    // Replace placeholders with real text
    text = text
            .replaceAll("%y", newsletter.getYear().toString())
            .replaceAll("%e", newsletter.getEdition())
            .replaceAll("%u", deploymentUrl + "/mail/"
                    + newsletter.getDeployedFilename());

    // Automatically escape links like described at
    // http://daringfireball.net/projects/markdown/syntax#autoescape
    // (one way or another, pegdown doesn't do this for us)
    text = autoEscape(text);

    // Use the markdown processor to parse all other elements...
    if (hardwraps) {
      html = PEGDOWN_HARDWRAPS.markdownToHtml(text);
    } else {
      html = PEGDOWN_NORMAL.markdownToHtml(text);
    }

    // ...replace hard space and hard return symbols with HTML...
    html = html
            .replaceAll("␣", "&nbsp;")
            .replaceAll("↵", "<br />");

    // ...and replace some HTML
    return html.replaceAll("</p>\\n<p>", "<br /><br />")
            .replaceAll("<p>", "")
            .replaceAll("</p>", "")
            .replaceAll("<em>", "<i>")
            .replaceAll("</em>", "</i>")
            .replaceAll("<strong>", "<b>")
            .replaceAll("</strong>", "</b>")
            .replaceAll("<table>", "<table cellpadding=\"0\" cellspacing=\"0\">")
            .replaceAll("<t(h|d)( |>)", "<t$1 style=\"padding-right: 1em\"$2");
  }

  private String parseImages(String markdownText) {
    // NOTE: only inline image syntax is supported!
    Matcher m = ImageExtractor.IMAGE_PARSER.matcher(markdownText);
    StringBuffer b = new StringBuffer();

    while (m.find()) {
      String location;
      String classString = "";
      String style = "";
      String altText;
      String titleText;
      Image image = imageMap.get(Long.parseLong(m.group(2)));

      if (local) {
        location = "../../../../static/uploads/newsletter/image/" + image.getLocalFilename();
      } else {
        location = deploymentUrl + "/image/" + image.getDeployedFilename();
      }

      if (image.getWidth() == 608) {
        classString = " class=\"header-img\"";
      } else if (image.getWidth() < 33 && image.getHeight() < 33) {
        style = " style=\"display:inline-block;vertical-align:sub\"";
      } else {
        classString = " class=\"body-img\"";
        if (image.getHeight() % 21 != 0) {
          style = " style=\"padding-top: " + (21 - (image.getHeight() % 21)) + "px\"";
        }
      }

      if ("".equals(m.group(1))) {
        altText = (image.getAltText() != null ? image.getAltText() : "");
      } else {
        altText = m.group(1);
      }

      if (m.group(4) == null) {
        titleText = image.getTitleText();
      } else {
        titleText = m.group(4);
      }

      // NOTE: width and height are integers, so they don't need to be encoded
      String replacement = "<img src=\"" + Encode.forHtmlAttribute(location)
              + "\" width=\"" + image.getWidth() + "\" height=\"" + image.getHeight()
              + "\" alt=\"" + Encode.forHtmlAttribute(altText) + "\"";

      if (titleText != null && !titleText.equals("")) {
        replacement += " title=\"" + Encode.forHtmlAttribute(titleText) + "\"";
      }

      replacement += classString + style + " />"
              + mdItalicized(image.getDescription());
      m.appendReplacement(b, replacement);
    }

    m.appendTail(b);
    return b.toString();
  }

  private String autoEscape(String markdownText) {
    // NOTE: only inline link syntax is supported
    // and the only transformations done are:
    // & -> &amp;
    //   -> %20
    Matcher m = LINK_PARSER.matcher(markdownText);
    StringBuffer b = new StringBuffer();

    while (m.find()) {
      String replacement = "[" + m.group(1) + "]("
              + m.group(2).replaceAll("&(?!amp;)", "&amp;").replace(" ", "%20");

      if (m.group(5) != null) {
        replacement += " " + m.group(4) + m.group(5) + m.group(4);
      }

      replacement += ")";
      m.appendReplacement(b, replacement);
    }

    m.appendTail(b);
    return b.toString();
  }

  private String welcomeMessage(String contents) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 8, "center", "outer", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("624", 8, "", "fullwidth", "");
    html += beginRow();
    html += beginCell("", "font: 9.5pt/21px 'Segoe UI', Verdana, sans-serif");
    html += contents;
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String viewOnline(String contents) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 8, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("624", 0, "", "fullwidth", "");
    html += beginRow();
    html += beginCell("", "text-align: right; font: 8.5pt/15px 'Segoe UI', Verdana, sans-serif; background: #efefef; padding-right: 8px");
    html += contents;
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String headerOrFooter(String contents) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 8, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("624", 8, "", "fullwidth", "");
    html += beginRow();
    html += beginCell("", "text-align: center; font: 8.5pt/15px 'Segoe UI', Verdana, sans-serif; background: #efefef");
    html += contents;
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String headLevel1(String head) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 0, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("fullwidthcell", "padding: 0 8px; width: 624px");
    html += beginTable("624", 8, "", "fullwidth", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("608", 8, "", "innertable", "");
    html += beginRow();
    html += beginCell("", "font: 16pt/21px 'Segoe UI', Verdana, sans-serif");
    html += head;
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String headLevel2(String head, String subhead) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 0, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("fullwidthcell", "padding: 0 8px; width: 624px");
    html += beginTable("624", 8, "", "fullwidth", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("608", 8, "", "innertable", "");
    html += beginRow();
    html += beginCell("", "font: 13pt/21px 'Segoe UI', Verdana, sans-serif");
    html += head;
    if (!subhead.equals("")) {
      html += "<br /><span style=\"font: 9.5pt/21px 'Segoe UI', Verdana, sans-serif\">"
              + subhead
              + "</span>";
    }
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String contentRow(String mainContents, String sidebarContents) {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 0, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("fullwidthcell", "padding: 0 8px; width: 624px");
    html += beginTable("624", 8, "", "fullwidth", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    if (sidebarContents.equals("")) {
      html += beginTable("608", 8, "", "innertable", "");
    } else {
      html += beginTable("316", 8, "left", "innertable", "");
    }
    html += beginRow();
    html += beginCell("", "font: 9.5pt/21px 'Segoe UI', Verdana, sans-serif");
    html += mainContents;
    html += endCell();
    html += endRow();
    html += endTable();
    if (!sidebarContents.equals("")) {
      html += beginTable("292", 8, "right", "innertable", "");
      html += beginRow();
      html += beginCell("", "font: 9.5pt/21px 'Segoe UI', Verdana, sans-serif");
      html += sidebarContents;
      html += endCell();
      html += endRow();
      html += endTable();
    }
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String rulerRow() {
    String html = "";
    html += beginTable("100%", 0, "", "backgroundTable", "background: #efefef");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("640", 0, "center", "outer", "background: #efefef");
    html += beginRow();
    html += beginCell("fullwidthcell", "padding: 0 8px; width: 624px");
    html += beginTable("624", 8, "", "fullwidth", "background: #fff");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("608", 8, "", "innertable", "");
    html += beginRow();
    html += beginCell("", "");
    html += beginTable("592", 0, "", "ruler", "");
    html += beginRow();
    html += "<td height=\"1\" style=\"border-bottom:1px solid #ccc\"></td>";
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    html += endCell();
    html += endRow();
    html += endTable();
    return html;
  }

  private String head(String title) {
    return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
            + "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
            + "<head>"
            + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
            + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />"
            + "<title>" + title + "</title>"
            + "<style type=\"text/css\">"
            + "#outlook a {padding:0}"
            + "body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0}"
            + ".ExternalClass {width:100%}"
            + ".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%}"
            + "p {margin: 0; padding: 0; font-size: 0px; line-height: 0px}"
            + ".backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important}"
            + "table td {border-collapse: collapse}"
            + "table {border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt}"
            + "img {display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic}"
            + "a img {border: none}"
            + "a {color: #2233FF}"
            + "@media only screen and (max-device-width: 480px) {"
            + " table[class=\"outer\"] {width:320px !important}"
            + " td[class=\"fullwidthcell\"] {padding:0 10px !important;width:300px !important;}"
            + " table[class=\"fullwidth\"] {padding:12px !important;width:284px !important}"
            + " table[class=\"innertable\"] {width:284px !important}"
            + " table[class=\"ruler\"] {width:268px !important}"
            + " img[class=\"header-img\"] {max-width:292px !important;height:auto !important}"
            + " img[class=\"body-img\"] {max-width:268px !important;height:auto !important;padding-top:0 !important}"
            + "}"
            + "</style>"
            + "</head>";
  }

  private String beginTable(String width, int cellPadding, String align, String className, String style) {
    String html = "<table";
    html += " width=\"" + width + "\"";
    html += " cellpadding=\"" + cellPadding + "\"";
    html += " cellspacing=\"0\" border=\"0\"";
    if (!align.equals("")) {
      html += " align=\"" + align + "\"";
    }
    if (!className.equals("")) {
      html += " class=\"" + className + "\"";
    }
    if (!style.equals("")) {
      html += " style=\"" + style + "\"";
    }
    html += ">";
    return html;
  }

  private String endTable() {
    return "</table>";
  }

  private String beginRow() {
    return "<tr>";
  }

  private String endRow() {
    return "</tr>";
  }

  private String beginCell(String className, String style) {
    String html = "<td";
    if (!className.equals("")) {
      html += " class=\"" + className + "\"";
    }
    if (!style.equals("")) {
      html += " style=\"" + style + "\"";
    }
    html += ">";
    return html;
  }

  private String endCell() {
    return "</td>";
  }
}
