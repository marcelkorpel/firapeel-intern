package nl.marcelkorpel.newsletter.domain;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.mail.PasswordAuthentication;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Image;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItem;

/**
 *
 * @author mk
 */
public class SendMail {

  public static void doSendMail(DefaultText defaultText, Newsletter newsletter,
          List<NewsletterItem> items, Map<Long, Image> imageMap,
          String recipients, String welcomeText) throws MessagingException, UnsupportedEncodingException {
    Config cfg = new Config();
    String protocol = cfg.getProperty("smtp_protocol", true);
    String hostname = cfg.getProperty("smtp_hostname", true);
    String starttls = cfg.getProperty("smtp_starttls", true);
    String port = cfg.getProperty("smtp_port", true);
    String username = cfg.getProperty("smtp_username", true);
    String password = cfg.getProperty("smtp_password", true);
    String from = cfg.getProperty("mail_from_address", true);

    Parser parser = new Parser(defaultText, newsletter, items, imageMap, false);

    String plainText = parser.createNewsletterPlainText(welcomeText, true);
    String html = parser.createNewsletterHTML(welcomeText, true);

    Properties properties = new Properties();
    properties.put("mail.transport.protocol", protocol);
    properties.put("mail." + protocol + ".auth", "true");
    properties.put("mail." + protocol + ".host", hostname);
    properties.put("mail." + protocol + ".starttls", starttls);

    Session session = Session.getDefaultInstance(properties);

    MimeMessage message = new MimeMessage(session);
    message.setFrom(new InternetAddress(from, defaultText.getFromName(), "UTF-8"));
    message.setReplyTo(InternetAddress.parse(defaultText.getReplyToAddress()));
    message.setRecipients(Message.RecipientType.BCC,
            InternetAddress.parse(recipients));
    message.setSubject(newsletter.getSubject()
            .replace("%y", newsletter.getYear().toString())
            .replace("%e", newsletter.getEdition()));
    message.setSentDate(new Date());

    MimeMultipart multipart = new MimeMultipart("alternative");
    MimeBodyPart textPart = new MimeBodyPart();
    textPart.setText(plainText, "utf-8");
    MimeBodyPart htmlPart = new MimeBodyPart();
    htmlPart.setContent(html, "text/html; charset=utf-8");

    // add parts
    multipart.addBodyPart(textPart);
    multipart.addBodyPart(htmlPart);

    message.setContent(multipart);
    Transport transport = session.getTransport();
    transport.connect(hostname, Integer.parseInt(port), username, password);
    transport.sendMessage(message, message.getAllRecipients());
    transport.close();
  }
}
