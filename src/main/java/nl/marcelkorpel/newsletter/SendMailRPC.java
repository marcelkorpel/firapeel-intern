package nl.marcelkorpel.newsletter;

import nl.marcelkorpel.newsletter.domain.Parser;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.config.Config;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.dao.NewsletterItemDAO;
import nl.marcelkorpel.dao.RecipientDAO;
import static nl.marcelkorpel.newsletter.deployment.Deployer.deployHTML;
import static nl.marcelkorpel.newsletter.deployment.Deployer.deployImages;
import static nl.marcelkorpel.newsletter.domain.SendMail.doSendMail;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.model.NewsletterItem;
import nl.marcelkorpel.newsletter.model.Recipient;
import nl.marcelkorpel.newsletter.rpc.SendMailParams;
import nl.marcelkorpel.newsletter.rpc.SendMailResponse;
import static nl.marcelkorpel.newsletter.util.Util.calculateMaxTimestamp;
import static nl.marcelkorpel.util.Util.listToString;
import static nl.marcelkorpel.newsletter.util.ImageExtractor.retrieveImageMap;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/sendmail-rpc")
public class SendMailRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    Gson gson = new Gson();
    String json = request.getParameter("data");
    SendMailParams params = gson.fromJson(json, SendMailParams.class);
    SendMailResponse res = new SendMailResponse();
    response.setContentType("application/json;charset=UTF-8");
    List<Recipient> list;

    try {
      DAOFactory db = DAOFactory.getInstance();
      RecipientDAO recipientDAO = db.getRecipientDAO();

      switch (params.getAction()) {
        case "add":
          String[] recipients = params.getAddresses().split("\\s*[\\r?\\n|,|;]\\s*");

          recipientDAO.insertBatch(recipients);

          list = recipientDAO.list();
          res.setRecipients(list);
          res.setOk(1);
          break;
        case "delete":
          long id = Long.parseLong(params.getRecipientId());
          recipientDAO.delete(id);

          list = recipientDAO.list();
          res.setRecipients(list);
          res.setOk(1);
          break;
        case "send":
          Config cfg = new Config();
          int max = Integer.parseInt(cfg.getProperty("smtp_max_per_batch", true));
          list = recipientDAO.getBatch(max);

          if (!list.isEmpty()) {
            DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
            NewsletterDAO newsletterDAO = db.getNewsletterDAO();
            NewsletterItemDAO newsletterItemDAO = db.getNewsletterItemDAO();
            DefaultText defaultText = defaultTextDAO.getDefaultText();
            Newsletter newsletter = newsletterDAO.getNewsletter(params.getNewsletterId());
            List<NewsletterItem> items = newsletterItemDAO.getItems(newsletter.getId());
            Map imageMap = retrieveImageMap(defaultText, items);
            deployImages(imageMap);

            if (newsletter.getDeployedFilename() == null
                    || newsletter.getUpdateTS() < calculateMaxTimestamp(imageMap)) {
              Parser parser = new Parser(defaultText, newsletter, items, imageMap, false);
              String onlineHTML = parser.createNewsletterHTML();
              deployHTML(newsletter, onlineHTML);
            }

            doSendMail(defaultText, newsletter, items, imageMap,
                    listToString(list, ","), params.getWelcomeText());

            long first = list.get(0).getId();
            long last = list.get(list.size() - 1).getId();
            recipientDAO.deleteBatch(first, last);
            list = recipientDAO.list();
          }

          res.setRecipients(list);
          res.setOk(1);
      }
    } catch (DAOException | MessagingException | UnsupportedEncodingException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
