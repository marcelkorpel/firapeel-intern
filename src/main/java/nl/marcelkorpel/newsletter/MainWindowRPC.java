/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static nl.marcelkorpel.accounts.Domain.getCurrentUserId;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.newsletter.model.DefaultText;
import nl.marcelkorpel.newsletter.model.Newsletter;
import nl.marcelkorpel.newsletter.rpc.NewsletterParams;
import nl.marcelkorpel.newsletter.rpc.NewsletterResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter-rpc")
public class MainWindowRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    Gson gson = new Gson();
    String json = request.getParameter("data");
    NewsletterParams params = gson.fromJson(json, NewsletterParams.class);
    NewsletterResponse res = new NewsletterResponse();
    response.setContentType("application/json;charset=UTF-8");
    Newsletter newsletter;

    try {
      DAOFactory db = DAOFactory.getInstance();
      NewsletterDAO newsletterDAO = db.getNewsletterDAO();

      switch (params.getAction()) {
        case "new":
          DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
          DefaultText defaultText = defaultTextDAO.getDefaultText();
          Newsletter lastNewsletter = newsletterDAO.getLast();
          Integer edition;
          try {
            edition = Integer.parseInt(lastNewsletter.getEdition());
          } catch (NumberFormatException e) {
            edition = -1;
          }
          Integer year = lastNewsletter.getYear();
          newsletter = new Newsletter();
          newsletter.setUserId(getCurrentUserId(request));
          if (year == null) {     // no last record present, set default to
            newsletter.setYear(3000);   // just a year in the future
          } else {
            newsletter.setYear(year);
          }

          // Create non-existing year/edition combination
          Newsletter test;
          do {
            edition++;
            newsletter.setEdition(edition.toString());
            newsletter.setSubject(defaultText.getSubject());
            test = newsletterDAO.getNewsletter(newsletter.getYear(),
                    newsletter.getEdition());
          } while (test.getId() != null);

          newsletterDAO.create(newsletter);
          res.setNewsletter(newsletter);
          res.setOk(1);
          break;
        case "save":
          newsletter = params.getNewsletter();
          newsletter.setUserId(getCurrentUserId(request));
          newsletterDAO.update(newsletter);
          res.setOk(1);
          break;
        default:
          res.setException("Illegal RPC operation");
      }
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
