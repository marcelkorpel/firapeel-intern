package nl.marcelkorpel.newsletter.rpc;

import nl.marcelkorpel.newsletter.model.Image;

public class ImageUploadResponse {

  private int ok;
  private String exception;
  private Image image;

  // Getters and setters
  public int getOk() {
    return ok;
  }

  public void setOk(int ok) {
    this.ok = ok;
  }

  public String getException() {
    return exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }

  public Image getImage() {
    return image;
  }

  public void setImage(Image image) {
    this.image = image;
  }
}
