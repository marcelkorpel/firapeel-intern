package nl.marcelkorpel.newsletter.rpc;

import nl.marcelkorpel.newsletter.model.Newsletter;

public class NewsletterParams {

  private String action;
  private Newsletter newsletter;

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public Newsletter getNewsletter() {
    return newsletter;
  }

  public void setNewsletter(Newsletter newsletter) {
    this.newsletter = newsletter;
  }
}
