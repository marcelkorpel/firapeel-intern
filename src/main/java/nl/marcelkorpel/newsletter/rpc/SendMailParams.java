package nl.marcelkorpel.newsletter.rpc;

/**
 *
 * @author mk
 */
public class SendMailParams {

  private String action;
  private String addresses;
  private String welcomeText;
  private long newsletterId;
  private String recipientId;

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getAddresses() {
    return addresses;
  }

  public void setAddresses(String addresses) {
    this.addresses = addresses;
  }

  public String getWelcomeText() {
    return welcomeText;
  }

  public void setWelcomeText(String welcomeText) {
    this.welcomeText = welcomeText;
  }

  public long getNewsletterId() {
    return newsletterId;
  }

  public void setNewsletterId(long newsletterId) {
    this.newsletterId = newsletterId;
  }

  public String getRecipientId() {
    return recipientId;
  }

  public void setRecipientId(String recipientId) {
    this.recipientId = recipientId;
  }
}
