package nl.marcelkorpel.newsletter.rpc;

import nl.marcelkorpel.newsletter.model.Newsletter;

public class NewsletterResponse {

  private int ok;
  private String exception;
  private Newsletter newsletter;

  public int getOk() {
    return ok;
  }

  public void setOk(int ok) {
    this.ok = ok;
  }

  public String getException() {
    return exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }

  public Newsletter getNewsletter() {
    return newsletter;
  }

  public void setNewsletter(Newsletter newsletter) {
    this.newsletter = newsletter;
  }
}
