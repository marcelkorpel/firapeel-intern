package nl.marcelkorpel.newsletter.rpc;

public class DefaultPostResponse {

  private int ok;
  private long lastChanged;
  private String exception;

  public int getOk() {
    return ok;
  }

  public void setOk(int ok) {
    this.ok = ok;
  }

  public long getLastChanged() {
    return lastChanged;
  }

  public void setLastChanged(long lastChanged) {
    this.lastChanged = lastChanged;
  }

  public String getException() {
    return exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }
}
