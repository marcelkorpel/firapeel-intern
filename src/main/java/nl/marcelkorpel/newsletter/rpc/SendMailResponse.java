package nl.marcelkorpel.newsletter.rpc;

import java.util.List;
import nl.marcelkorpel.newsletter.model.Recipient;

/**
 *
 * @author mk
 */
public class SendMailResponse {

  private int ok;
  private String exception;
  private List<Recipient> recipients;

  public int getOk() {
    return ok;
  }

  public void setOk(int ok) {
    this.ok = ok;
  }

  public String getException() {
    return exception;
  }

  public void setException(String exception) {
    this.exception = exception;
  }

  public List<Recipient> getRecipients() {
    return recipients;
  }

  public void setRecipients(List<Recipient> recipients) {
    this.recipients = recipients;
  }
}
