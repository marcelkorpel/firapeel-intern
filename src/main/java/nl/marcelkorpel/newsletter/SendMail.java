package nl.marcelkorpel.newsletter;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.DefaultTextDAO;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.dao.RecipientDAO;
import nl.marcelkorpel.newsletter.model.SendMailData;

/**
 *
 * @author mk
 */
@WebServlet("/app/newsletter/sendmail")
public class SendMail extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    NewsletterDAO newsletterDAO = db.getNewsletterDAO();
    RecipientDAO recipientDAO = db.getRecipientDAO();
    DefaultTextDAO defaultTextDAO = db.getDefaultTextDAO();
    SendMailData data = new SendMailData();
    data.setNewsletters(newsletterDAO.list());
    data.setRecipients(recipientDAO.list());
    data.setWelcomeText(defaultTextDAO.getDefaultText().getWelcomeText());
    Gson gson = new Gson();
    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("data", gson.toJson(data));
    request.getRequestDispatcher("/WEB-INF/newsletter-sendmail.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
