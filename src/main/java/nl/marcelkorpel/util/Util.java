package nl.marcelkorpel.util;

import java.util.List;
import java.util.StringJoiner;

/**
 *
 * @author mk
 */
public class Util {

  public static String listToString(List<? extends Object> list, String delimeter) {
    StringJoiner sj = new StringJoiner(delimeter);

    for (Object element : list) {
      sj.add(element.toString());
    }

    return sj.toString();
  }

  public static String createInClause(int size) {
    StringBuilder sql = new StringBuilder();
    sql.append("IN (");

    for (int i = 0; i < size; i++) {
      if (i > 0) {
        sql.append(',');
      }
      sql.append('?');
    }

    sql.append(")");
    return sql.toString();
  }
}
