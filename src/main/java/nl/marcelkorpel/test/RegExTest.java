package nl.marcelkorpel.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExTest {
  private static String autoEscape(String markdownText) {
    // NOTE: only inline link syntax is supported
    // and the only transformations done are:
    // & -> &amp;
    //   -> %20
    // RegEx based on http://www.metaltoad.com/blog/regex-quoted-string-escapable-quotes
    Matcher m = Pattern
            .compile("\\[(.+)\\]\\(((.(?![\"']))+)(?:\\s*([\"'])((?:.(?!(?<![\\\\])\\4))*.?)\\4)?\\)")
            .matcher(markdownText);
    StringBuffer b = new StringBuffer();

    while (m.find()) {
      System.out.println(m.group(1));
      System.out.println(m.group(2));
      System.out.println(m.group(3));
      System.out.println(m.group(4));
      System.out.println(m.group(5));
      String replacement = "[" + m.group(1) + "]("
              + m.group(2).trim().replaceAll("&(?!amp;)", "&amp;").replace(" ", "%20");

      if (m.group(5) != null) {
        replacement += " " + m.group(4) + m.group(5) + m.group(4);
      }

      replacement += ")";
      m.appendReplacement(b, replacement);
    }

    m.appendTail(b);
    return b.toString();
  }

  public static void main(String[] args) {
    System.out.println(autoEscape("[fff](http://images.google.com/images?num=30&q=larry+bird\"Titel\")"));
  }
}
