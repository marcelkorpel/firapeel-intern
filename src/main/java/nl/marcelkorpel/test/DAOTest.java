package nl.marcelkorpel.test;

import java.util.List;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.dao.NewsletterDAO;
import nl.marcelkorpel.newsletter.model.Newsletter;

public class DAOTest {

  public static void main(String[] args) throws Exception {
    DAOFactory intern = DAOFactory.getInstance();
    System.out.println("DAOFactory successfully obtained: " + intern);

    NewsletterDAO newsletterDAO = intern.getNewsletterDAO();
    System.out.println("NewsletterDAO successfully obtained: " + newsletterDAO);

    Newsletter newsletter = new Newsletter();
    newsletter.setYear(2015);
    newsletter.setEdition("4");
    newsletter.setSubject("Nieuwsbrief 2015/4");
    newsletterDAO.create(newsletter);
    System.out.println("Newsletter succesfully created: " + newsletter);

    // Create another newsletter.
    Newsletter anotherNewsletter = new Newsletter();
    anotherNewsletter.setYear(2016);
    anotherNewsletter.setEdition("0");
    anotherNewsletter.setSubject("Nieuwsbrief 2016/0");
    newsletterDAO.create(anotherNewsletter);
    System.out.println("Another newsletter successfully created: " + anotherNewsletter);

    // Update newsletter.
    newsletter.setEdition("5");
    newsletter.setSubject("Nieuwsbrief 2015/5");
    newsletterDAO.update(newsletter);
    System.out.println("Newsletter successfully updated: " + newsletter);

    // List all newsletters.
    List<Newsletter> newsletters = newsletterDAO.list();
    System.out.println("List of newsletters successfully queried: " + newsletters);
    System.out.println("Thus, amount of newsletters in database is: " + newsletters.size());

    // Delete newsletter.
    newsletterDAO.delete(newsletter);
    System.out.println("Newsletter successfully deleted: " + newsletter);

    // Delete another newsletter.
    newsletterDAO.delete(anotherNewsletter);
    System.out.println("Another newsletter successfully deleted: " + anotherNewsletter);

    // List all newsletters again.
    newsletters = newsletterDAO.list();
    System.out.println("List of newsletters successfully queried: " + newsletters);
    System.out.println("Thus, amount of newsletters in database is: " + newsletters.size());
  }
}
