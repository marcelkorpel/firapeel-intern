/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;
import nl.marcelkorpel.newsletter.model.Newsletter;

/**
 *
 * @author mk
 */
public class NewsletterDAOJDBC implements NewsletterDAO {

  // Constants
  private static final String SQL_SELECT_BY_ID
          = "SELECT newsletter_id, user_id, deployed_filename, year, edition, "
          + "subject, update_ts FROM nws_newsletter WHERE newsletter_id = ?";
  private static final String SQL_SELECT_BY_YEAR_EDITION
          = "SELECT newsletter_id, user_id, deployed_filename, year, edition, "
          + "subject, update_ts FROM nws_newsletter WHERE year = ? AND edition = ?";
  private static final String SQL_LIST_ORDER_BY_ID
          = "SELECT newsletter_id, user_id, deployed_filename, year, edition, "
          + "subject, update_ts FROM nws_newsletter ORDER BY newsletter_id";
  private static final String SQL_SELECT_LAST
          = "SELECT newsletter_id, user_id, deployed_filename, year, edition, "
          + "subject, update_ts FROM nws_newsletter ORDER BY newsletter_id DESC LIMIT 1";
  private static final String SQL_INSERT
          = "INSERT INTO nws_newsletter (user_id, deployed_filename, year, "
          + "edition, subject, update_ts) VALUES (?, ?, ?, ?, ?, UNIX_TIMESTAMP())";
  private static final String SQL_UPDATE
          = "UPDATE nws_newsletter SET user_id = ?, deployed_filename = ?, "
          + "year = ?, edition = ?, subject = ?, update_ts = UNIX_TIMESTAMP() "
          + "WHERE newsletter_id = ?";
  private static final String SQL_REMOVE_DEPLOYED_FILENAMES
          = "UPDATE nws_newsletter SET deployed_filename = NULL";
  private static final String SQL_DELETE
          = "DELETE FROM nws_newsletter WHERE newsletter_id = ?";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  NewsletterDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public Newsletter getNewsletter(long id) throws DAOException {
    Newsletter newsletter = new Newsletter();
    Object[] values = {
      id
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_SELECT_BY_ID, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        newsletter = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return newsletter;
  }

  @Override
  public Newsletter getNewsletter(int year, String edition) throws DAOException {
    Newsletter newsletter = new Newsletter();
    Object[] values = {
      year,
      edition
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_SELECT_BY_YEAR_EDITION, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        newsletter = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return newsletter;
  }

  @Override
  public List<Newsletter> list() throws DAOException {
    List<Newsletter> newsletters = new ArrayList<>();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        newsletters.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return newsletters;
  }

  @Override
  public Newsletter getLast() throws DAOException {
    Newsletter newsletter = new Newsletter();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_SELECT_LAST);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        newsletter = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return newsletter;
  }

  @Override
  public void create(Newsletter newsletter) throws IllegalArgumentException, DAOException {
    if (newsletter.getId() != null) {
      throw new IllegalArgumentException("Newsletter is already created, the newsletter ID is not null.");
    }

    Object[] values = {
      newsletter.getUserId(),
      newsletter.getDeployedFilename(),
      newsletter.getYear(),
      newsletter.getEdition(),
      newsletter.getSubject()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_INSERT, true, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Creating newsletter failed, no rows affected.");
      }

      try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          newsletter.setId(generatedKeys.getLong(1));
          newsletter.setUpdateTS(System.currentTimeMillis() / 1000L);
        } else {
          throw new DAOException("Creating newsletter failed, no generated key obtained.");
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void update(Newsletter newsletter) throws IllegalArgumentException, DAOException {
    if (newsletter.getId() == null) {
      throw new IllegalArgumentException("Newsletter is not created yet, the newsletter ID is null.");
    }

    Object[] values = {
      newsletter.getUserId(),
      newsletter.getDeployedFilename(),
      newsletter.getYear(),
      newsletter.getEdition(),
      newsletter.getSubject(),
      newsletter.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_UPDATE, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Updating newsletter failed, no rows affected.");
      } else {
        newsletter.setUpdateTS(System.currentTimeMillis() / 1000L);
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void removeDeployedFilenames() throws IllegalArgumentException, DAOException {

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_REMOVE_DEPLOYED_FILENAMES);) {
      stmt.executeUpdate();
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void delete(Newsletter newsletter) throws DAOException {
    Object[] values = {
      newsletter.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_DELETE, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Deleting newsletter failed, no rows affected.");
      } else {
        newsletter.setId(null);
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static Newsletter map(ResultSet resultSet) throws SQLException {
    Newsletter newsletter = new Newsletter();
    newsletter.setId(resultSet.getLong("newsletter_id"));
    newsletter.setUserId(resultSet.getLong("user_id"));
    newsletter.setDeployedFilename(resultSet.getString("deployed_filename"));
    newsletter.setYear(resultSet.getInt("year"));
    newsletter.setEdition(resultSet.getString("edition"));
    newsletter.setSubject(resultSet.getString("subject"));
    newsletter.setUpdateTS(resultSet.getLong("update_ts"));
    return newsletter;
  }
}
