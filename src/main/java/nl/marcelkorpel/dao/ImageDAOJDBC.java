package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;
import nl.marcelkorpel.newsletter.model.Image;
import static nl.marcelkorpel.util.Util.createInClause;

public class ImageDAOJDBC implements ImageDAO {

  // Constants
  private static final String SQL_SELECT_BY_ID
          = "SELECT image_id, name, local_filename, deployed_filename, "
          + "width, height, alt_text, title_text, description, update_ts "
          + "FROM nws_image WHERE image_id = ?";
  private static final String SQL_LIST_ORDER_BY_ID
          = "SELECT image_id, name, local_filename, deployed_filename, "
          + "width, height, alt_text, title_text, description, update_ts "
          + "FROM nws_image ORDER BY image_id";
  private static final String SQL_RETRIEVE_FROM_SET_FIRST_PART
          = "SELECT image_id, name, local_filename, deployed_filename, "
          + "width, height, alt_text, title_text, description, update_ts "
          + "FROM nws_image WHERE image_id ";
  private static final String SQL_INSERT_AFTER_UPLOAD
          = "INSERT INTO nws_image (local_filename, width, height, update_ts) "
          + "VALUES (?, ?, ?, UNIX_TIMESTAMP())";
  private static final String SQL_INSERT_AFTER_SAVE
          = "INSERT INTO nws_image (name, alt_text, title_text, description, "
          + "update_ts) "
          + "VALUES (?, ?, ?, ?, UNIX_TIMESTAMP())";
  private static final String SQL_UPDATE_AFTER_UPLOAD
          = "UPDATE nws_image SET local_filename = ?, deployed_filename = NULL, "
          + "width = ?, height = ?, update_ts = UNIX_TIMESTAMP() "
          + "WHERE image_id = ?";
  private static final String SQL_UPDATE_AFTER_SAVE
          = "UPDATE nws_image SET name = ?, alt_text = ?, title_text = ?, "
          + "description = ?, update_ts = UNIX_TIMESTAMP() WHERE image_id = ?";
  private static final String SQL_UPDATE_DEPLOYED_FILENAMES
          = "UPDATE nws_image SET deployed_filename = ?, "
          + "update_ts = UNIX_TIMESTAMP() WHERE image_id = ?";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  ImageDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public Image retrieveImage(long id) throws DAOException {
    Image image = new Image();
    Object[] values = {
      id
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_SELECT_BY_ID, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        image = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return image;
  }

  @Override
  public List<Image> list() throws DAOException {
    List<Image> list = new ArrayList<>();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        list.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return list;
  }

  @Override
  public Map<Long, Image> retrieveImageMap(Set<Long> ids) throws DAOException {
    Map<Long, Image> map = new HashMap<>();
    String sql = SQL_RETRIEVE_FROM_SET_FIRST_PART;
    sql += createInClause(ids.size());

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);) {
      int i = 1;
      for (Long id : ids) {
        stmt.setLong(i++, id);
      }
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        map.put(resultSet.getLong("image_id"), map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return map;
  }

  @Override
  public void createAfterUpload(Image image) throws IllegalArgumentException, DAOException {
    if (image.getId() != null) {
      throw new IllegalArgumentException("Image is already created, the image ID is not null.");
    }

    Object[] values = {
      image.getLocalFilename(),
      image.getWidth(),
      image.getHeight()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_INSERT_AFTER_UPLOAD, true, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Creating image failed, no rows affected.");
      }

      try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          image.setId(generatedKeys.getLong(1));
          image.setUpdateTS(System.currentTimeMillis() / 1000L);
        } else {
          throw new DAOException("Creating newsletter failed, no generated key obtained.");
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void createAfterSave(Image image) throws IllegalArgumentException, DAOException {
    if (image.getId() != null) {
      throw new IllegalArgumentException("Image is already created, the image ID is not null.");
    }

    Object[] values = {
      image.getName(),
      image.getAltText(),
      image.getTitleText(),
      image.getDescription()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_INSERT_AFTER_SAVE, true, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Creating image failed, no rows affected.");
      }

      try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          image.setId(generatedKeys.getLong(1));
          image.setUpdateTS(System.currentTimeMillis() / 1000L);
        } else {
          throw new DAOException("Creating newsletter failed, no generated key obtained.");
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void updateAfterUpload(Image image) throws IllegalArgumentException, DAOException {
    if (image.getId() == null) {
      throw new IllegalArgumentException("Image is not created yet, the image ID is null.");
    }

    Object[] values = {
      image.getLocalFilename(),
      image.getWidth(),
      image.getHeight(),
      image.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_UPDATE_AFTER_UPLOAD, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Updating image failed, no rows affected.");
      } else {
        image.setUpdateTS(System.currentTimeMillis() / 1000L);
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void updateAfterSave(Image image) throws IllegalArgumentException, DAOException {
    if (image.getId() == null) {
      throw new IllegalArgumentException("Image is not created yet, the image ID is null.");
    }

    Object[] values = {
      image.getName(),
      image.getAltText(),
      image.getTitleText(),
      image.getDescription(),
      image.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_UPDATE_AFTER_SAVE, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Updating image failed, no rows affected.");
      } else {
        image.setUpdateTS(System.currentTimeMillis() / 1000L);
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void updateDeployedFilenames(Map<Long, Image> imageMap) throws DAOException {
    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_UPDATE_DEPLOYED_FILENAMES);) {
      for (Image image : imageMap.values()) {
        stmt.setString(1, image.getDeployedFilename());
        stmt.setLong(2, image.getId());
        image.setUpdateTS(System.currentTimeMillis() / 1000L);
        stmt.addBatch();
      }
      stmt.executeBatch();
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static Image map(ResultSet resultSet) throws SQLException {
    Image image = new Image();
    image.setId(resultSet.getLong("image_id"));
    image.setName(resultSet.getString("name"));
    image.setLocalFilename(resultSet.getString("local_filename"));
    image.setDeployedFilename(resultSet.getString("deployed_filename"));
    image.setWidth(resultSet.getInt("width"));
    image.setHeight(resultSet.getInt("height"));
    image.setAltText(resultSet.getString("alt_text"));
    image.setTitleText(resultSet.getString("title_text"));
    image.setDescription(resultSet.getString("description"));
    image.setUpdateTS(resultSet.getLong("update_ts"));
    return image;
  }
}
