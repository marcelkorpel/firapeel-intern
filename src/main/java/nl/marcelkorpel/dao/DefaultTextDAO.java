/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import nl.marcelkorpel.newsletter.model.DefaultText;

/**
 *
 * @author mk
 */
public interface DefaultTextDAO {

  // Actions
  public DefaultText getDefaultText() throws DAOException;

  public void setDefaultText(DefaultText defaultText) throws DAOException;
}
