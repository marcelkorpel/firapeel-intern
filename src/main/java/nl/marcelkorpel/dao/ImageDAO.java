package nl.marcelkorpel.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;
import nl.marcelkorpel.newsletter.model.Image;

public interface ImageDAO {

  // Actions
  public Image retrieveImage(long id) throws DAOException;

  public List<Image> list() throws DAOException;

  public Map<Long, Image> retrieveImageMap(Set<Long> ids) throws DAOException;

  public void createAfterUpload(Image image) throws IllegalArgumentException, DAOException;

  public void createAfterSave(Image image) throws IllegalArgumentException, DAOException;

  public void updateAfterUpload(Image image) throws IllegalArgumentException, DAOException;

  public void updateAfterSave(Image image) throws IllegalArgumentException, DAOException;

  public void updateDeployedFilenames(Map<Long, Image> imageMap) throws DAOException;
}
