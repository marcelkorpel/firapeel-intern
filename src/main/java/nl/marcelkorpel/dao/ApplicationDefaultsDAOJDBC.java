package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nl.marcelkorpel.application.model.ApplicationDefaults;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;

/**
 *
 * @author mk
 */
public class ApplicationDefaultsDAOJDBC implements ApplicationDefaultsDAO {

  // Constants
  private static final String SQL_SELECT
          = "SELECT application_name FROM application_defaults "
          + "WHERE application_defaults_id = 1";
  private static final String SQL_COUNT
          = "SELECT COUNT(*) FROM application_defaults";
  private static final String SQL_INSERT
          = "INSERT INTO application_defaults "
          + "(application_defaults_id, application_name) VALUES (1, ?)";
  private static final String SQL_UPDATE
          = "UPDATE application_defaults SET application_name = ? "
          + "WHERE application_defaults_id = 1";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  ApplicationDefaultsDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public ApplicationDefaults getApplicationDefaults() throws DAOException {
    ApplicationDefaults applicationDefaults = new ApplicationDefaults();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_SELECT);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {      // row available
        applicationDefaults = map(resultSet);
      } else {      // row not available, set fields to empty strings
        applicationDefaults.setApplicationName("");
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return applicationDefaults;
  }

  @Override
  public void setApplicationDefaults(ApplicationDefaults applicationDefaults) throws DAOException {

    Object[] values = {
      applicationDefaults.getApplicationName()
    };

    // First, check if the singleton is present in the table
    try (
            Connection connection = daoFactory.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_COUNT);) {
      resultSet.next();
      if (resultSet.getInt(1) == 0) {   // row not present, insert
        try (
                PreparedStatement pstmt = prepareStatement(connection, SQL_INSERT, false, values);) {
          int affectedRows = pstmt.executeUpdate();

          if (affectedRows == 0) {
            throw new DAOException("Setting ApplicationDefaults failed, no rows affected.");
          }
        } catch (SQLException e) {
          throw new DAOException(e);
        }
      } else {                          // row present, update
        try (
                PreparedStatement pstmt = prepareStatement(connection, SQL_UPDATE, false, values);) {
          int affectedRows = pstmt.executeUpdate();

          if (affectedRows == 0) {
            throw new DAOException("Setting ApplicationDefaults failed, no rows affected.");
          }
        } catch (SQLException e) {
          throw new DAOException(e);
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static ApplicationDefaults map(ResultSet resultSet) throws SQLException {
    ApplicationDefaults applicationDefaults = new ApplicationDefaults();
    applicationDefaults.setApplicationName(resultSet.getString("application_name"));
    return applicationDefaults;
  }
}
