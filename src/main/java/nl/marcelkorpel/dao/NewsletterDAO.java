/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.util.List;
import nl.marcelkorpel.newsletter.model.Newsletter;

/**
 *
 * @author mk
 */
public interface NewsletterDAO {

  // Actions
  public Newsletter getNewsletter(long id) throws DAOException;

  public Newsletter getNewsletter(int year, String edition) throws DAOException;

  public List<Newsletter> list() throws DAOException;

  public Newsletter getLast() throws DAOException;

  public void create(Newsletter newsletter) throws IllegalArgumentException, DAOException;

  public void update(Newsletter newsletter) throws IllegalArgumentException, DAOException;

  public void removeDeployedFilenames() throws IllegalArgumentException, DAOException;

  public void delete(Newsletter newsletter) throws DAOException;
}
