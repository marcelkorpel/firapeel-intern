/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import nl.marcelkorpel.accounts.domain.AccountsError;
import nl.marcelkorpel.accounts.model.User;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;

/**
 *
 * @author Marcel Korpel <marcel.korpel@gmail.com>
 */
public class UserDAOJDBC implements UserDAO {

  private static final String SQL_LIST
          = "SELECT user_id, username, passwordhash, full_name, email, "
          + "HEX(reset_key) AS resetkey, update_ts, active "
          + "FROM act_user WHERE active = 1 ORDER BY user_id";
  private static final String SQL_UNIQUE_EMAIL
          = "SELECT COUNT(*) FROM act_user WHERE email = ?";
  private static final String SQL_INSERT_USER
          = "INSERT INTO act_user (email, reset_key, update_ts) VALUES "
          + "(?, UNHEX(?), UNIX_TIMESTAMP())";
  private static final String SQL_RESET_KEY
          = "SELECT user_id, username, passwordhash, full_name, email, "
          + "HEX(reset_key) AS resetkey, update_ts, active "
          + "FROM act_user WHERE HEX(reset_key) = ?";
  private static final String SQL_RETRIEVE_USER
          = "SELECT user_id, username, passwordhash, full_name, email, "
          + "HEX(reset_key) AS resetkey, update_ts, active "
          + "FROM act_user WHERE username = ? AND active = 1";
  private static final String SQL_RETRIEVE_BY_EMAIL
          = "SELECT user_id, username, passwordhash, full_name, email, "
          + "HEX(reset_key) AS resetkey, update_ts, active "
          + "FROM act_user WHERE email = ? AND active = 1";
  private static final String SQL_SET_RESET_KEY
          = "UPDATE act_user SET reset_key = UNHEX(?) WHERE user_id = ?";
  private static final String SQL_UNIQUE_USERNAME
          = "SELECT COUNT(*) FROM act_user "
          + "WHERE username = ? AND user_id <> ?";
  private static final String SQL_SET_USERNAME_PASSWORDHASH
          = "UPDATE act_user SET username = ?, passwordhash = ?, "
          + "reset_key = NULL, update_ts = UNIX_TIMESTAMP() "
          + "WHERE user_id = ?";
  private static final String SQL_UNIQUE_USERNAME_EMAIL
          = "SELECT COUNT(*) FROM act_user "
          + "WHERE (username = ? OR email = ?) AND user_id <> ?";
  private static final String SQL_UPDATE_USER
          = "UPDATE act_user SET username = ?, full_name = ?, email = ?, "
          + "update_ts = UNIX_TIMESTAMP() WHERE user_id = ?";
  private static final String SQL_INACTIVATE_USER
          = "UPDATE act_user SET passwordhash = NULL, email = NULL, "
          + "reset_key = NULL, update_ts = UNIX_TIMESTAMP(), active = 0 "
          + "WHERE user_id = ?";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  UserDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public List<User> list() throws DAOException {
    List<User> users = new ArrayList<>();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_LIST);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        users.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return users;
  }

  @Override
  public void newUser(User user) throws DAOException, AccountsError {
    if (user.getId() != null) {
      throw new IllegalArgumentException("User is already created, the user ID is not null.");
    }

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_UNIQUE_EMAIL);) {
      stmt.setString(1, user.getEmail());
      ResultSet resultSet = stmt.executeQuery();
      resultSet.next();
      if (resultSet.getInt(1) == 1) {
        throw new AccountsError("Email address is already in use.");
      } else {
        Object[] values = {
          user.getEmail(),
          user.getResetKey()
        };

        try (PreparedStatement istmt = prepareStatement(connection, SQL_INSERT_USER, true, values);) {
          int affectedRows = istmt.executeUpdate();
          if (affectedRows == 0) {
            throw new DAOException("Creating user failed, no rows affected.");
          }
          try (ResultSet generatedKeys = istmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
              user.setId(generatedKeys.getLong(1));
              user.setUpdateTS(System.currentTimeMillis() / 1000L);
              user.setActive(1);
            } else {
              throw new DAOException("Creating user failed, no generated key obtained.");
            }
          }
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public User getUserByResetKey(String resetKey) throws DAOException {
    User user = new User();

    Object[] values = {
      resetKey
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_RESET_KEY, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        user = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return user;
  }

  @Override
  public User getUserByUsername(String username) throws DAOException {
    User user = new User();

    Object[] values = {
      username
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_RETRIEVE_USER, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        user = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return user;
  }

  @Override
  public User getUserByEmail(String email) throws DAOException {
    User user = new User();

    Object[] values = {
      email
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_RETRIEVE_BY_EMAIL, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        user = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return user;
  }

  @Override
  public void setResetKey(User user) throws DAOException {
    if (user.getId() == null) {
      throw new IllegalArgumentException("User is not yet created, the user ID is null.");
    }

    Object[] updValues = {
      user.getResetKey(),
      user.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_SET_RESET_KEY, false, updValues);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Updating user failed, no rows affected.");
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void setUsernamePasswordHash(User user) throws DAOException, AccountsError {
    if (user.getId() == null) {
      throw new IllegalArgumentException("User is not yet created, the user ID is null.");
    }

    Object[] values = {
      user.getUsername(),
      user.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_UNIQUE_USERNAME, false, values);
            ResultSet resultSet = stmt.executeQuery();) {
      resultSet.next();
      if (resultSet.getInt(1) == 1) {
        throw new AccountsError("Username is already in use.");
      } else {
        Object[] updValues = {
          user.getUsername(),
          user.getPasswordHash(),
          user.getId()
        };

        try (PreparedStatement ustmt = prepareStatement(connection, SQL_SET_USERNAME_PASSWORDHASH, true, updValues);) {
          int affectedRows = ustmt.executeUpdate();
          if (affectedRows == 0) {
            throw new DAOException("Updating user failed, no rows affected.");
          } else {
            user.setResetKey(null);
            user.setUpdateTS(System.currentTimeMillis() / 1000L);
          }
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void updateUser(User user) throws DAOException, AccountsError {
    if (user.getId() == null) {
      throw new IllegalArgumentException("User is not yet created, the user ID is null.");
    }

    Object[] values = {
      user.getUsername(),
      user.getEmail(),
      user.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_UNIQUE_USERNAME_EMAIL, false, values);
            ResultSet resultSet = stmt.executeQuery();) {
      resultSet.next();
      if (resultSet.getInt(1) == 1) {
        throw new AccountsError("Username and email address must be unique.");
      } else {
        Object[] updValues = {
          user.getUsername(),
          user.getFullName(),
          user.getEmail(),
          user.getId()
        };

        try (PreparedStatement ustmt = prepareStatement(connection, SQL_UPDATE_USER, false, updValues);) {
          int affectedRows = ustmt.executeUpdate();
          if (affectedRows == 0) {
            throw new DAOException("Updating user failed, no rows affected.");
          } else {
            user.setUpdateTS(System.currentTimeMillis() / 1000L);
          }
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void inactivateUser(User user) throws DAOException {
    Object[] values = {
      user.getId()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_INACTIVATE_USER, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Updating user failed, no rows affected.");
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static User map(ResultSet resultSet) throws SQLException {
    User user = new User();
    user.setId(resultSet.getLong("user_id"));
    user.setUsername(resultSet.getString("username"));
    user.setPasswordHash(resultSet.getString("passwordhash"));
    user.setFullName(resultSet.getString("full_name"));
    user.setEmail(resultSet.getString("email"));
    user.setResetKey(resultSet.getString("resetkey"));
    user.setUpdateTS(resultSet.getLong("update_ts"));
    user.setActive(resultSet.getInt("active"));
    return user;
  }
}
