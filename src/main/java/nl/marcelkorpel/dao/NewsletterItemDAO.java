/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.util.List;
import nl.marcelkorpel.newsletter.model.NewsletterItem;

/**
 *
 * @author mk
 */
public interface NewsletterItemDAO {

  // Actions
  public List<NewsletterItem> getItems(long newsletterId) throws DAOException;

  public void setItems(long newsletterId, List<NewsletterItem> itemList) throws DAOException;
}
