package nl.marcelkorpel.dao;

import java.util.List;
import nl.marcelkorpel.accounts.domain.AccountsError;
import nl.marcelkorpel.accounts.model.User;

public interface UserDAO {

  // Actions
  List<User> list() throws DAOException;

  public void newUser(User user) throws DAOException, AccountsError;

  public User getUserByResetKey(String resetKey) throws DAOException;

  public User getUserByUsername(String username) throws DAOException;

  public User getUserByEmail(String email) throws DAOException;

  public void setResetKey(User user) throws DAOException;

  public void setUsernamePasswordHash(User user) throws DAOException, AccountsError;

  public void updateUser(User user) throws DAOException, AccountsError;

  public void inactivateUser(User user) throws DAOException;
}
