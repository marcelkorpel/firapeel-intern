package nl.marcelkorpel.dao;

import java.util.List;
import nl.marcelkorpel.accounts.model.Session;
import nl.marcelkorpel.accounts.model.User;

public interface SessionDAO {

  // Actions
  public Session getSession(String id);

  public List<Session> getSessions(long userId);

  public void insertSession(Session session) throws DAOException;

  public void inactivateSession(String id) throws DAOException;

  public void inactivateSessions(User user) throws DAOException;
}
