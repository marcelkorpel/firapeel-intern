/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.SQLException;
import nl.marcelkorpel.config.Config;

/**
 *
 * @author mk
 */
public abstract class DAOFactory {

  // Constants
  private static final String DRIVER = "db_driver";
  private static final String DSN_PREFIX = "db_dsn_prefix";
  private static final String HOST = "db_host";
  private static final String PORT = "db_port";
  private static final String NAME = "db_name";
  private static final String USERNAME = "db_username";
  private static final String PASSWORD = "db_password";

  // Actions
  public static DAOFactory getInstance() {
    Config properties = new Config();
    String driverClassName = properties.getProperty(DRIVER, true);
    String prefix = properties.getProperty(DSN_PREFIX, true);
    String host = properties.getProperty(HOST, true);
    String port = properties.getProperty(PORT, true);
    String name = properties.getProperty(NAME, true);
    String url = "jdbc:" + prefix + "://" + host + ":" + port + "/" + name
            + "?useUnicode=yes&characterEncoding=UTF-8";
    String password = properties.getProperty(PASSWORD, false);
    String username = properties.getProperty(USERNAME, password != null);
    DAOFactory instance;

    try {
      Class.forName(driverClassName);
    } catch (ClassNotFoundException ex) {
      throw new DAOException("Driver class '" + driverClassName
              + "' is missing in classpath.", ex);
    }

    instance = new DriverManagerDAOFactory(url, username, password);
    return instance;
  }

  abstract Connection getConnection() throws SQLException;

  public ApplicationDefaultsDAOJDBC getApplicationDefaultsDAOJDBC() {
    return new ApplicationDefaultsDAOJDBC(this);
  }

  public UserDAOJDBC getUserDAOJDBC() {
    return new UserDAOJDBC(this);
  }

  public SessionDAOJDBC getSessionDAOJDBC() {
    return new SessionDAOJDBC(this);
  }

  public NewsletterDAOJDBC getNewsletterDAO() {
    return new NewsletterDAOJDBC(this);
  }

  public NewsletterItemDAOJDBC getNewsletterItemDAO() {
    return new NewsletterItemDAOJDBC(this);
  }

  public DefaultTextDAOJDBC getDefaultTextDAO() {
    return new DefaultTextDAOJDBC(this);
  }

  public ImageDAOJDBC getImageDAO() {
    return new ImageDAOJDBC(this);
  }

  public RecipientDAOJDBC getRecipientDAO() {
    return new RecipientDAOJDBC(this);
  }
}
