/*
 * To change this license head_texter, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;
import nl.marcelkorpel.newsletter.model.NewsletterItem;

/**
 *
 * @author mk
 */
public class NewsletterItemDAOJDBC implements NewsletterItemDAO {

  // Constants
  private static final String SQL_LIST_ITEMS
          = "SELECT newsletter_item_id, head_level, head_text, subhead_text, "
          + "body_text, sidebar_text, metadata_text FROM nws_newsletter_item "
          + "WHERE newsletter_id = ? ORDER BY newsletter_item_id";
  private static final String SQL_DELETE_ITEMS
          = "DELETE FROM nws_newsletter_item WHERE newsletter_id = ?";
  private static final String SQL_INSERT_ITEMS
          = "INSERT INTO nws_newsletter_item "
          + "(newsletter_id, head_level, head_text, subhead_text, body_text, "
          + "sidebar_text, metadata_text) VALUES (?, ?, ?, ?, ?, ?, ?)";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  NewsletterItemDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public List<NewsletterItem> getItems(long newsletterId) throws DAOException {
    List<NewsletterItem> items = new ArrayList<>();

    Object[] values = {
      newsletterId
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_LIST_ITEMS, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        items.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return items;
  }

  @Override
  public void setItems(long newsletterId, List<NewsletterItem> itemList) throws DAOException {
    Object[] values = {
      newsletterId
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_DELETE_ITEMS, false, values);) {
      stmt.executeUpdate();
      // do not check # of affected rows here, it might legitimately be zero
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    for (NewsletterItem item : itemList) {
      Object[] itemValues = {
        newsletterId,
        item.getHeadLevel(),
        item.getHead(),
        item.getSubhead(),
        item.getBodyText(),
        item.getSidebarText(),
        item.getMetadataText()
      };

      // this is of course far from optimized, refactor later!
      try (
              Connection connection = daoFactory.getConnection();
              PreparedStatement stmt = prepareStatement(connection, SQL_INSERT_ITEMS, false, itemValues);) {
        int affectedRows = stmt.executeUpdate();
        if (affectedRows == 0) {
          throw new DAOException("Creating newsletterItem failed, no rows affected.");
        }
      } catch (SQLException e) {
        throw new DAOException(e);
      }
    }
  }

  // Helper function
  private static NewsletterItem map(ResultSet resultSet) throws SQLException {
    NewsletterItem item = new NewsletterItem();
    item.setId(resultSet.getLong("newsletter_item_id"));
    item.setHeadLevel(resultSet.getInt("head_level"));
    item.setHead(resultSet.getString("head_text"));
    item.setSubhead(resultSet.getString("subhead_text"));
    item.setBodyText(resultSet.getString("body_text"));
    item.setSidebarText(resultSet.getString("sidebar_text"));
    item.setMetadataText(resultSet.getString("metadata_text"));
    return item;
  }
}
