package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;
import nl.marcelkorpel.newsletter.model.Recipient;

/**
 *
 * @author mk
 */
public class RecipientDAOJDBC implements RecipientDAO {

  // Constants
  private static final String SQL_LIST
          = "SELECT recipient_id, address FROM nws_recipient ORDER BY address";
  private static final String SQL_LIST_LIMIT
          = "SELECT recipient_id, address FROM nws_recipient ORDER BY recipient_id LIMIT ?";
  private static final String SQL_DELETE
          = "DELETE FROM nws_recipient WHERE recipient_id = ?";
  private static final String SQL_DELETE_ITEMS
          = "DELETE FROM nws_recipient WHERE recipient_id BETWEEN ? AND ?";
  private static final String SQL_INSERT_ITEMS
          = "INSERT IGNORE INTO nws_recipient (recipient_group_id, address) "
          + "VALUES (1, ?)";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  RecipientDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public List<Recipient> list() throws DAOException {
    List<Recipient> recipients = new ArrayList<>();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_LIST);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        recipients.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return recipients;
  }

  @Override
  public List<Recipient> getBatch(int max) throws DAOException {
    List<Recipient> recipients = new ArrayList<>();

    Object[] values = {
      max
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_LIST_LIMIT, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        recipients.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return recipients;
  }

  @Override
  public void delete(long recipient_id) throws DAOException {
    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_DELETE);) {
      stmt.setLong(1, recipient_id);
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Deleting recipient failed, no rows affected.");
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void deleteBatch(long first, long last) throws DAOException {
    Object[] values = {
      first,
      last
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_DELETE_ITEMS, false, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Deleting recipients failed, no rows affected.");
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void insertBatch(String[] recipients) throws DAOException {
    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_INSERT_ITEMS);) {
      for (String recipient : recipients) {
        stmt.setString(1, recipient);
        stmt.addBatch();
      }
      stmt.executeBatch();
      // Don't check if rows are affected, items may be duplicate ones
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static Recipient map(ResultSet resultSet) throws SQLException {
    Recipient recipient = new Recipient();
    recipient.setId(resultSet.getLong("recipient_id"));
    recipient.setAddress(resultSet.getString("address"));
    return recipient;
  }
}
