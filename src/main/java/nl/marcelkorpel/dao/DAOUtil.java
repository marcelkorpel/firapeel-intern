/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mk
 */
public final class DAOUtil {

  // Constructor
  private DAOUtil() {
    // Utility class, hide constructor
  }

  // Actions
  public static PreparedStatement prepareStatement(Connection connection, String sql, boolean returnGeneratedKeys, Object... values)
          throws SQLException {
    PreparedStatement statement = connection.prepareStatement(sql,
            returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
    setValues(statement, values);
    return statement;
  }

  public static void setValues(PreparedStatement statement, Object... values)
          throws SQLException {
    for (int i = 0; i < values.length; i++) {
      statement.setObject(i + 1, values[i]);
    }
  }
}
