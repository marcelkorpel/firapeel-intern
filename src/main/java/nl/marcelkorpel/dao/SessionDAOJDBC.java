package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import nl.marcelkorpel.accounts.model.Session;
import nl.marcelkorpel.accounts.model.User;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;

public class SessionDAOJDBC implements SessionDAO {

  private static final String SQL_RETRIEVE_SESSION = "SELECT "
          + "HEX(session_id) AS sessionid, user_id, provider_name, remote_host, "
          + "location, update_ts, active FROM act_session "
          + "WHERE HEX(session_id) = ? AND active = 1";
  private static final String SQL_RETRIEVE_LAST_SESSIONS = "SELECT "
          + "HEX(session_id) AS sessionid, user_id, provider_name, remote_host, "
          + "location, update_ts, active FROM act_session "
          + "WHERE user_id = ? ORDER BY session_id DESC LIMIT 10";
  private static final String SQL_INSERT_SESSION = "INSERT INTO act_session "
          + "(session_id, user_id, provider_name, remote_host, location, update_ts) "
          + "VALUES (UNHEX(?), ?, NULL, ?, NULL, UNIX_TIMESTAMP())";
  private static final String SQL_INACTIVATE_SESSION = "UPDATE act_session "
          + "SET active = 0 WHERE HEX(session_id) = ?";
  private static final String SQL_INACTIVATE_SESSIONS = "UPDATE act_session "
          + "SET active = 0 WHERE user_id = ?";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  SessionDAOJDBC(DAOFactory daoFactory) throws DAOException {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public Session getSession(String id) {
    Session session = new Session();

    Object[] values = {
      id
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_RETRIEVE_SESSION, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {
        session = map(resultSet);
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return session;
  }

  @Override
  public List<Session> getSessions(long userId) {
    List<Session> list = new ArrayList<>();

    Object[] values = {
      userId
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_RETRIEVE_LAST_SESSIONS, true, values);
            ResultSet resultSet = stmt.executeQuery();) {
      while (resultSet.next()) {
        list.add(map(resultSet));
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return list;
  }

  @Override
  public void insertSession(Session session) throws DAOException {
    Object[] values = {
      session.getId(),
      session.getUserId(),
      session.getRemoteHost()
    };

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = prepareStatement(connection, SQL_INSERT_SESSION, true, values);) {
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Creating session failed, no rows affected.");
      }
      session.setUpdateTS(System.currentTimeMillis() / 1000L);
      session.setActive(1);
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void inactivateSession(String id) throws DAOException {
    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_INACTIVATE_SESSION);) {
      stmt.setString(1, id);
      int affectedRows = stmt.executeUpdate();
      if (affectedRows == 0) {
        throw new DAOException("Deleting session failed, no rows affected.");
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  @Override
  public void inactivateSessions(User user) throws DAOException {
    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_INACTIVATE_SESSIONS);) {
      stmt.setLong(1, user.getId());
      stmt.executeUpdate();
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static Session map(ResultSet resultSet) throws SQLException {
    Session session = new Session();
    session.setId(resultSet.getString("sessionid"));
    session.setUserId(resultSet.getLong("user_id"));
    session.setProvider(resultSet.getString("provider_name"));
    session.setRemoteHost(resultSet.getString("remote_host"));
    session.setLocation(resultSet.getString("location"));
    session.setUpdateTS(resultSet.getLong("update_ts"));
    session.setActive(resultSet.getInt("active"));
    return session;
  }
}
