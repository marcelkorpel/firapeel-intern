package nl.marcelkorpel.dao;

import nl.marcelkorpel.application.model.ApplicationDefaults;

/**
 *
 * @author mk
 */
public interface ApplicationDefaultsDAO {

  // Actions
  public ApplicationDefaults getApplicationDefaults() throws DAOException;

  public void setApplicationDefaults(ApplicationDefaults applicationDefaults) throws DAOException;
}
