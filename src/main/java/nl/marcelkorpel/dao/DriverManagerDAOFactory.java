/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mk
 */
public class DriverManagerDAOFactory extends DAOFactory {

  private final String url;
  private final String username;
  private final String password;

  DriverManagerDAOFactory(String url, String username, String password) {
    this.url = url;
    this.username = username;
    this.password = password;
  }

  @Override
  Connection getConnection() throws SQLException {
    return DriverManager.getConnection(url, username, password);
  }
}
