package nl.marcelkorpel.dao;

import java.util.List;
import nl.marcelkorpel.newsletter.model.Recipient;

/**
 *
 * @author mk
 */
public interface RecipientDAO {

  // Actions
  public List<Recipient> list() throws DAOException;

  public List<Recipient> getBatch(int max) throws DAOException;

  public void delete(long id) throws DAOException;

  public void deleteBatch(long first, long last) throws DAOException;

  public void insertBatch(String[] recipients) throws DAOException;
}
