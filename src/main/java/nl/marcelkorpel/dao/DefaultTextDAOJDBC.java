/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.marcelkorpel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static nl.marcelkorpel.dao.DAOUtil.prepareStatement;
import nl.marcelkorpel.newsletter.model.DefaultText;

/**
 *
 * @author mk
 */
public class DefaultTextDAOJDBC implements DefaultTextDAO {

  // Constants
  private static final String SQL_SELECT
          = "SELECT from_name, reply_to_address, subject, plain_text_online, "
          + "html_online, header_text, colophon_head, colophon_text, "
          + "footer_text, welcome_text FROM nws_default_text "
          + "WHERE default_text_id = 1";
  private static final String SQL_COUNT
          = "SELECT COUNT(*) FROM nws_default_text";
  private static final String SQL_INSERT
          = "INSERT INTO nws_default_text (default_text_id, from_name, "
          + "reply_to_address, subject, plain_text_online, html_online, "
          + "header_text, colophon_head, colophon_text, footer_text, "
          + "welcome_text) VALUES (1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String SQL_UPDATE
          = "UPDATE nws_default_text SET from_name = ?, reply_to_address = ?, "
          + "subject = ?, plain_text_online = ?, html_online = ?, header_text = ?, "
          + "colophon_head = ?, colophon_text = ?, footer_text = ?, "
          + "welcome_text = ? WHERE default_text_id = 1";

  // Vars
  private final DAOFactory daoFactory;

  // Constructors
  DefaultTextDAOJDBC(DAOFactory daoFactory) {
    this.daoFactory = daoFactory;
  }

  // Actions
  @Override
  public DefaultText getDefaultText() throws DAOException {
    DefaultText defaultText = new DefaultText();

    try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement stmt = connection.prepareStatement(SQL_SELECT);
            ResultSet resultSet = stmt.executeQuery();) {
      if (resultSet.next()) {      // row available
        defaultText = map(resultSet);
      } else {      // row not available, set fields to empty strings
        defaultText.setFromName("");
        defaultText.setReplyToAddress("");
        defaultText.setSubject("");
        defaultText.setPlainTextOnline("");
        defaultText.setHtmlOnline("");
        defaultText.setHeaderText("");
        defaultText.setColophonHead("");
        defaultText.setColophonText("");
        defaultText.setFooterText("");
        defaultText.setWelcomeText("");
      }
    } catch (SQLException ex) {
      throw new DAOException(ex);
    }

    return defaultText;
  }

  @Override
  public void setDefaultText(DefaultText defaultText) throws DAOException {

    Object[] values = {
      defaultText.getFromName(),
      defaultText.getReplyToAddress(),
      defaultText.getSubject(),
      defaultText.getPlainTextOnline(),
      defaultText.getHtmlOnline(),
      defaultText.getHeaderText(),
      defaultText.getColophonHead(),
      defaultText.getColophonText(),
      defaultText.getFooterText(),
      defaultText.getWelcomeText()
    };

    // First, check if the singleton is present in the table
    try (
            Connection connection = daoFactory.getConnection();
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_COUNT);) {
      resultSet.next();
      if (resultSet.getInt(1) == 0) {   // row not present, insert
        try (
                PreparedStatement pstmt = prepareStatement(connection, SQL_INSERT, false, values);) {
          int affectedRows = pstmt.executeUpdate();

          if (affectedRows == 0) {
            throw new DAOException("Setting DefaultText failed, no rows affected.");
          }
        } catch (SQLException e) {
          throw new DAOException(e);
        }
      } else {                          // row present, update
        try (
                PreparedStatement pstmt = prepareStatement(connection, SQL_UPDATE, false, values);) {
          int affectedRows = pstmt.executeUpdate();

          if (affectedRows == 0) {
            throw new DAOException("Setting DefaultText failed, no rows affected.");
          }
        } catch (SQLException e) {
          throw new DAOException(e);
        }
      }
    } catch (SQLException e) {
      throw new DAOException(e);
    }
  }

  // Helper function
  private static DefaultText map(ResultSet resultSet) throws SQLException {
    DefaultText defaultText = new DefaultText();
    defaultText.setFromName(resultSet.getString("from_name"));
    defaultText.setReplyToAddress(resultSet.getString("reply_to_address"));
    defaultText.setSubject(resultSet.getString("subject"));
    defaultText.setPlainTextOnline(resultSet.getString("plain_text_online"));
    defaultText.setHtmlOnline(resultSet.getString("html_online"));
    defaultText.setHeaderText(resultSet.getString("header_text"));
    defaultText.setColophonHead(resultSet.getString("colophon_head"));
    defaultText.setColophonText(resultSet.getString("colophon_text"));
    defaultText.setFooterText(resultSet.getString("footer_text"));
    defaultText.setWelcomeText(resultSet.getString("welcome_text"));
    return defaultText;
  }
}
