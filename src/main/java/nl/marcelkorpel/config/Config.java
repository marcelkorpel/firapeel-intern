package nl.marcelkorpel.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author mk
 */
public class Config {

  // Constants
  public static final String APPNAME = "firapeel-intern";
  public static final String STATIC_DIR = "/static";
  public static final String UPLOAD_DIR = STATIC_DIR + "/uploads";
  public static final String APPROOT;
  private static final String CONFIG_FILE;
  private static final Properties PROPERTIES = new Properties();

  static {
    InputStream propertiesFile;

    if (System.getenv("OPENSHIFT_DATA_DIR") == null) {
      APPROOT = System.getProperty("user.home") + "/.app-data/" + APPNAME;
    } else {
      APPROOT = System.getenv("OPENSHIFT_DATA_DIR") + APPNAME;
    }

    CONFIG_FILE = APPROOT + "/config.properties";

    try {
      propertiesFile = new FileInputStream(CONFIG_FILE);
      PROPERTIES.load(propertiesFile);
    } catch (IOException ex) {
      throw new ConfigurationException("Cannot load properties file '"
              + CONFIG_FILE + "'.", ex);
//      Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  // Constructors
  public Config() {
    /* deliberately empty */
  }

  // Actions
  /**
   * Get the application's root directory in the local file system
   *
   * @return String containing the root directory (without ending slash)
   */
  /*  public static String getApplicationRoot() {
    return APPROOT;
  }
   */
  /**
   * Get the value of a configuration property
   *
   * @param key Name of the property
   * @param mandatory Sets whether the returned property value should not be null nor empty
   * @return String containing value or ""
   */
  public String getProperty(String key, boolean mandatory) throws ConfigurationException {
    String property = PROPERTIES.getProperty(key);

    if (property == null || property.trim().length() == 0) {
      if (mandatory) {
        throw new ConfigurationException("Required property '" + key + "' "
                + "is missing in properties file '" + CONFIG_FILE + "'.");
      } else {
        // Make empty value null. Empty Strings are evil.
        property = null;
      }
    } else if (property.charAt(0) == '$') {
      // Resolve environment variable
      property = System.getenv(property.substring(1));
    }

    return property;
  }
}
