package nl.marcelkorpel.config;



public class ConfigProperty {

  final private String name;
  final private String description;
  private String value;

  public ConfigProperty(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
