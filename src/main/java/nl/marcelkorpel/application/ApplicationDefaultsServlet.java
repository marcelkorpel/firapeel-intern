package nl.marcelkorpel.application;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.application.model.ApplicationDefaults;
import nl.marcelkorpel.dao.ApplicationDefaultsDAO;
import nl.marcelkorpel.dao.DAOFactory;

/**
 *
 * @author mk
 */
@WebServlet("/app/application")
public class ApplicationDefaultsServlet extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DAOFactory db = DAOFactory.getInstance();
    ApplicationDefaultsDAO applicationDefaultsDAO = db.getApplicationDefaultsDAOJDBC();
    ApplicationDefaults applicationDefaults = applicationDefaultsDAO.getApplicationDefaults();
    response.setContentType("text/html; charset=UTF-8");
    request.setAttribute("application_name", applicationDefaults.getApplicationName());
    request.getRequestDispatcher("/WEB-INF/application-defaults.xhtml").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }
}
