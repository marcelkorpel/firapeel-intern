package nl.marcelkorpel.application;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.marcelkorpel.application.model.ApplicationDefaults;
import nl.marcelkorpel.dao.ApplicationDefaultsDAO;
import nl.marcelkorpel.dao.DAOException;
import nl.marcelkorpel.dao.DAOFactory;
import nl.marcelkorpel.newsletter.rpc.DefaultPostResponse;

/**
 *
 * @author mk
 */
@WebServlet("/app/application-rpc")
public class ApplicationDefaultsRPC extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    /* nothing */
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    DefaultPostResponse res = new DefaultPostResponse();
    Gson gson = new Gson();
    response.setContentType("application/json;charset=UTF-8");

    try {
      DAOFactory db = DAOFactory.getInstance();
      ApplicationDefaultsDAO applicationDefaultsDAO = db.getApplicationDefaultsDAOJDBC();
      String json = request.getParameter("data");
      ApplicationDefaults applicationDefaults = gson.fromJson(json, ApplicationDefaults.class);
      applicationDefaultsDAO.setApplicationDefaults(applicationDefaults);
      res.setOk(1);
    } catch (DAOException e) {
      res.setException(e.getMessage());
    } finally {
      response.getWriter().write(gson.toJson(res));
    }
  }
}
