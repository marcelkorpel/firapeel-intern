package nl.marcelkorpel.application.model;

/**
 *
 * @author mk
 */
public class ApplicationDefaults {

  // Properties
  private String applicationName;

  // Getters/setters
  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }
}
