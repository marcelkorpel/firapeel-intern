-- This script inserts several defaults into the database and an initial
-- user account. NOTE: with this account you should immediately create a
-- real admin account, relogin using this newly created account and wipe
-- the initial account.
--
USE intern;

INSERT INTO act_user (user_id, username, passwordhash, email) VALUES (1, 'init', '', 'init@localhost');
INSERT INTO act_provider (provider_name) VALUES ('google');
INSERT INTO act_provider (provider_name) VALUES ('facebook');
INSERT INTO act_provider (provider_name) VALUES ('microsoft');
INSERT INTO nws_recipient_group (recipient_group_id, group_name) VALUES (1, '(Default)');
